using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Reflection;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    // Scene IDs:
    private static readonly int MAIN_SCENE = 0;
    private static readonly int LEVEL_SCENE = 1;

    // This is for handling singletons properly:
    public static GameManager instance = null;


    /**
     * This is called when the game starts for the first time. Called once and
     * only once
     */
    void Awake()
    {
        Debug.Log("GameManager: Awake");
        if(HandleSingleton())
        {

        }
    }


    void Start()
    {
        Debug.Log("GameManager: Start");
    }


    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        Debug.Log(mode);
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a game manager!
            Debug.LogWarning("Second GameManager created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        Debug.Log("Setting GameManager to DontDestroyOnLoad");
        DontDestroyOnLoad(gameObject); 

        return true;
    }


    public static void LoadLevelScene()
    {
        Debug.Log("Loading Level Scene");
        SceneManager.LoadScene(LEVEL_SCENE);
    }


    public static void LoadMainScene()
    {
        Debug.Log("Loading Main Scene");
        SceneManager.LoadScene(MAIN_SCENE);
    }
}
