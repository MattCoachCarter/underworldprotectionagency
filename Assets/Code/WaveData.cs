using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class WaveData
{
    public MainCharacterData character_data;
    public int stipend;
    public DialogueData pre_wave_dialogue;
    public DialogueData post_wave_dialogue;
}
