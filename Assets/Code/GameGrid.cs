using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGrid
{
    private static readonly string SPACE_PREFAB = "Prefabs/Space";

    private int width = 0;
    private int height = 0;
    private int entrance_x = 0;
    private int exit_x = 0;
    private float y_offset = 0;
    private Space[,] spaces = null;
    private List<Tuple<int, int>> blocked_coordinates = null;


    public GameGrid(float y_off, int ent_x, int ex_x, List<Tuple<int, int>> blkd_coords)
    {
        y_offset = y_off;
        width = GameConfig.GRID_WIDTH;
        height = GameConfig.GRID_HEIGHT;
        spaces = new Space[height, width];
        entrance_x = ent_x;
        exit_x = ex_x;
        blocked_coordinates = blkd_coords;
        SpawnSpaceObjects();
    }


    public Vector3 GetMainCharacterSpawnPosition()
    {
        Space start_space = GetEntrance();
        float y_spawn_pos = start_space.transform.position.y + (GameConfig.SPACE_SIZE * 1.75f);
        return new Vector3(start_space.transform.position.x,
                           y_spawn_pos,
                           GameConfig.MAIN_CHARACTER_Z);
    }


    public void ArmAll()
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                spaces[x, y].Arm();
            }
        }
    }


    public void DisarmAll()
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                spaces[x, y].Disarm();
            }
        }
    }


    public Space[,] GetAllSpaces()
    {
        return spaces;
    }


    public void RemoveAllObjects()
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                spaces[x, y].RemoveObject();
            }
        }
    }


    public bool IsExit(Space maybe_exit)
    {
        return maybe_exit == GetExit();
    }


    public Space GetEntrance()
    {
        return GetSpace(GetEntranceX(), GetEntranceY());
    }


    public Space GetExit()
    {
        return GetSpace(GetExitX(), 0);
    }


    public int GetEntranceX()
    {
        return entrance_x;
    }


    public int GetExitX()
    {
        return exit_x;
    }


    public int GetEntranceY()
    {
        return (height - 1);
    }


    public bool IsInBounds(int x, int y)
    {
        if(x < 0)
        {
            return false;
        }
        else if(x >= width)
        {
            return false;
        }

        if(y < 0)
        {
            return false;
        }
        else if(y >= height)
        {
            return false;
        }

        return true;
    }


    public Space GetSpace(int x, int y)
    {
        if(!IsInBounds(x, y))
        {
            return null;
        }

        return spaces[x, y];
    }


    public List<Space> GetUnblockedNeighbors(Space space)
    {
        List<Space> unblocked_neighbor_spaces = new List<Space>();
        foreach(Space s in GetNeighbors(space))
        {
            if(!s.IsBlocked())
            {
                unblocked_neighbor_spaces.Add(s);
            }
        }

        return unblocked_neighbor_spaces;
    }


    public List<Space> GetNeighbors(Space space)
    {
        List<Space> neighbor_spaces = new List<Space>();
        Space left_space = GetSpaceInDirection(space, Direction.left);
        if(left_space != null)
        {
            neighbor_spaces.Add(left_space);
        }

        Space right_space = GetSpaceInDirection(space, Direction.right);
        if(right_space != null)
        {
            neighbor_spaces.Add(right_space);
        }

        Space up_space = GetSpaceInDirection(space, Direction.up);
        if(up_space != null)
        {
            neighbor_spaces.Add(up_space);
        }

        Space down_space = GetSpaceInDirection(space, Direction.down);
        if(down_space != null)
        {
            neighbor_spaces.Add(down_space);
        }

        return neighbor_spaces;
    }


    public Space GetSpaceInDirection(Space source_space, Direction dir)
    {
        int x_diff = 0;
        int y_diff = 0;
        switch(dir)
        {
            case Direction.up:
                y_diff = 1;
                break;
            case Direction.down:
                y_diff = -1;
                break;
            case Direction.left:
                x_diff = -1;
                break;
            case Direction.right:
                x_diff = 1;
                break;
        }

        int new_x = source_space.GetX() + x_diff;
        int new_y = source_space.GetY() + y_diff;
        if(IsInBounds(new_x, new_y))
        {
            return GetSpace(new_x, new_y);
        }
        return null;
    }


    public void Activate()
    {
        SetActivation(true);
    }


    public void Deactivate()
    {
        SetActivation(false);
    }


    private void SetActivation(bool activation_status)
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                spaces[x, y].gameObject.SetActive(activation_status);
            }
        }
    }


    private bool AreCoordsBlocked(int x, int y)
    {
        foreach(Tuple<int, int> blocked_coords in blocked_coordinates)
        {
            if(blocked_coords.Item1 == x && blocked_coords.Item2 == y)
            {
                return true;
            }
        }
        return false;
    }

    
    private void SpawnSpaceObjects()
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                float depth_offset = y / 10f;
                Vector3 spawn_position = new Vector3((float)(x * GameConfig.SPACE_SIZE),
                                                     (float)(y * GameConfig.SPACE_SIZE) + y_offset,
                                                     (float)(GameConfig.SPACE_Z + depth_offset));
                GameObject space_game_object = Common.Spawn(SPACE_PREFAB,
                                                            spawn_position);
                bool is_blocked_coord = AreCoordsBlocked(x, y);
                bool unplaceable = (x == GetEntranceX() && y == GetEntranceY()) ||
                                   (x == GetExitX() && y == 0) ||
                                   is_blocked_coord;
                bool permablocked = is_blocked_coord;

                Space space = space_game_object.GetComponent<Space>();
                space.Setup(this, x, y, unplaceable, permablocked);
                spaces[x, y] = space;
            }
        }
    }
}
