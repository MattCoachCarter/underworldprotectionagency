using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurtbox : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    [SerializeField] private bool is_trigger = false;

    private List<GameObject> hurt_objects;
    private bool player_controlled = false;
    private Action damage_callback = null;


    protected virtual void Awake()
    {
        hurt_objects = new List<GameObject>();
    }


    public bool IsTrigger()
    {
        return is_trigger;
    }


    public int GetDamage()
    {
        return damage;
    }


    public void SetDamage(int d)
    {
        damage = d;
    }


    public bool IsPlayerControlled()
    {
        return player_controlled;
    }


    public void SetupHurtbox(bool _player_controlled)
    {
        SetupHurtbox(_player_controlled, null);
    }


    public void SetupHurtbox(bool _player_controlled, Action _damage_callback)
    {
        player_controlled = _player_controlled;
        damage_callback = _damage_callback;
    }


    public void AddHurtObject(GameObject obj)
    {
        hurt_objects.Add(obj);
    }


    public bool AlreadyHurt(GameObject obj)
    {
        return hurt_objects.Contains(obj);
    }


    public int MaybeDamage(GameObject obj)
    {
        if(!AlreadyHurt(obj))
        {
            AddHurtObject(obj);
            if(damage_callback != null)
            {
                damage_callback();
            }
            return damage;
        }
        return 0;
    }
}
