using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DamageText : MonoBehaviour
{
    private static readonly float DMG_TXT_TTL = 0.75f;
    private static readonly float DMG_TXT_SPD = 10f;

    [SerializeField] private Text text;
    [SerializeField] private RectTransform rect_transform;

    private float time_alive = 0f;


    public void Setup(int damage_amount, Vector3 world_pos)
    {
        DoSetup(("-"+ damage_amount.ToString()), world_pos);
    }


    public void Setup(string txt, Vector3 world_pos)
    {
        DoSetup(txt, world_pos);
    }


    public void Setup(string txt, Vector3 world_pos, Color c)
    {
        DoSetup(txt, world_pos, c);
    }


    private void DoSetup(string txt, Vector3 world_pos)
    {
        DoSetup(txt, world_pos, new Color(1f, 1f, 1f));
    }


    private void DoSetup(string txt, Vector3 world_pos, Color c)
    {
        SetText(txt);
        rect_transform.localPosition = Common.WorldToScreenPosition(world_pos);
        text.color = new Color(c.r, c.g, c.b, 0.5f);
    }


    private void SetText(string txt)
    {
        text.text = txt;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time_alive += Time.deltaTime;
        Move();
        AdjustAlpha();

        if(time_alive >= DMG_TXT_TTL)
        {
            Common.SafeDestroy(gameObject);
        }
    }

    
    private void Move()
    {
        float speed_this_frame = DMG_TXT_SPD * Time.deltaTime;
        rect_transform.localPosition += new Vector3(0f, speed_this_frame, 0f);
    }
    

    private void AdjustAlpha()
    {
        float half_ttl = DMG_TXT_TTL / 2f;
        float pct_adjust = 0f;
        if(time_alive > half_ttl)
        {
            pct_adjust = (time_alive / 2f) / half_ttl;
        }

        float alpha = Mathf.Lerp(0.5f, 0, pct_adjust);
        text.color = new Color(1f, 1f, 1f, alpha);
    }
}
