using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class StoreObjectData
{
    public int id;
    public string name;
    public string description;
    public int price;
    public string sprite_name;
    public string prefab_name;
    public bool locked;
}
