using System;
using System.Collections;


[System.Serializable]
public class MainCharacterStats
{
    public int health;
    public int max_health;
    public int defense;
    public int attack;
    public float speed;


    public MainCharacterStats()
    {
        
    }
}
