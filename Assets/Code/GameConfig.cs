using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig
{
    // START HANDLING FOR SINGLETON
    private static GameConfig instance = new GameConfig();  // Calls constructor
    public static GameConfig Instance // Use to access the singleton
    {
        get { return instance; }
    }
    // END HANDLING FOR SINGLETON


    private GameConfig()  // Constructor, called in singleton logic
    {
        // Constructor
        // TODO: Load user config
    }


    ///////////////////////////////////////////////////////////////////////
    // Constant, not user settable:
    ///////////////////////////////////////////////////////////////////////
    public static bool DEBUG = false;


    // FILE/DATA MANAGEMENT
    public static readonly string GAME_DATA_FILE = "user_data.json";

    // VIEWPORT
    public static int X_RES = 960;
    public static int Y_RES = 640;

    // SPACE
    public static readonly int SPACE_SIZE = 16;
    public static readonly float SPACE_Z = 0f;
    public static readonly float HURTBOX_Z = -2f;

    // GRID
    public static readonly int GRID_WIDTH = 8;
    public static readonly int GRID_HEIGHT = 8;

    // MAIN CHARACTER
    public static readonly float MAIN_CHARACTER_Z = -2f;

    // VISUALS
    public static readonly float MAIN_CHARACTER_FADE_TIMING = 0.5f;
}
