using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneBurst : Hurtbox
{
    private static readonly float TTL = 1.3f;

    private float time_alive = 0f;
    private SpriteRenderer sprite_renderer;


    public void Setup(bool _player_controlled)
    {
        SetupHurtbox(_player_controlled);
    }


    // Start is called before the first frame update
    void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
        time_alive = 0f;
    }


    // Update is called once per frame
    void Update()
    {
        time_alive += Time.deltaTime;
        if(time_alive >= TTL)
        {
            Common.SafeDestroy(gameObject);
        }
        else
        {
            AdjustAlpha();
        }
    }


    private void AdjustAlpha()
    {
        float half_ttl = (TTL / 2f);
        float pct_adjustment = time_alive / half_ttl;
        float min = 0.25f;
        float max = 0.75f;
        if(time_alive >= half_ttl)
        {
            min = 0.75f;
            max = 0f;
            pct_adjustment = (time_alive / 2f) / half_ttl;
        }

        float new_alpha = Mathf.Lerp(min, max, pct_adjustment);
        sprite_renderer.color = new Color(1f, 1f, 1f, new_alpha);
    }
}
