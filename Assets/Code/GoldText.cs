using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GoldText : MonoBehaviour
{
    private static readonly float GOLD_TXT_TTL = 2.5f;
    private static readonly float GOLD_TXT_SPD = 10f;

    [SerializeField] private Text text;
    [SerializeField] private RectTransform rect_transform;

    private float time_alive = 0f;


    public void Setup(int gold_amount)
    {
        Setup(("+"+ gold_amount.ToString() +"  gold!"));
    }


    public void Setup(string txt)
    {
        SetText(txt);
        rect_transform.localPosition = new Vector3(0, 0, 0);
    }


    private void SetText(string txt)
    {
        text.text = txt;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time_alive += Time.deltaTime;
        Move();
        AdjustAlpha();

        if(time_alive >= GOLD_TXT_TTL)
        {
            Common.SafeDestroy(gameObject);
        }
    }

    
    private void Move()
    {
        float speed_this_frame = GOLD_TXT_SPD * Time.deltaTime;
        rect_transform.localPosition += new Vector3(0f, speed_this_frame, 0f);
    }
    

    private void AdjustAlpha()
    {
        float half_ttl = GOLD_TXT_TTL / 2f;
        float pct_adjust = 0f;
        if(time_alive > half_ttl)
        {
            pct_adjust = (time_alive / 2f) / half_ttl;
        }

        float alpha = Mathf.Lerp(1f, 0f, pct_adjust);
        text.color = new Color(1f, 1f, 1f, alpha);
    }
}
