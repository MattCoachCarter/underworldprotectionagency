using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueContinuationIndicator : MonoBehaviour
{
    private static float BLINK_TIME = 0.33f;

    [SerializeField] private Text text;
    private float timer = 0f;
    private bool showing = true;


    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= BLINK_TIME)
        {
            timer = 0f;
            if(showing)
            {
                text.enabled = false;
            }
            else
            {
                text.enabled = true;
            }
            showing = !showing;
        }
    }
}
