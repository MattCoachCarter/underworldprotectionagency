using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainCharacterUIUpdater : MonoBehaviour
{
    [SerializeField] private Text name_part_1;
    [SerializeField] private Text name_part_2;
    [SerializeField] private Text hp_text;


    // Update is called once per frame
    void Update()
    {
        MainCharacter character = LevelManager.GetCurrentMainCharacter();
        if(character != null)
        {
            MainCharacterData data = character.GetData();
            name_part_1.text = data.first_name;
            name_part_2.text = data.last_name;
            hp_text.text = data.stats.health +" / "+ data.stats.max_health;

        }
    }
}
