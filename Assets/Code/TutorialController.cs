using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    public static TutorialController instance = null;

    [SerializeField] private DialogueController main_dialogue_controller;
    [SerializeField] private DialogueController tutorial_dialogue_controller;
    [SerializeField] private UnderworldManager underworld_manager;
    [SerializeField] private UnderworldManager gary;
    [SerializeField] private GameObject pointer;
    [SerializeField] private GameObject main_ui;
    [SerializeField] private GameObject tutorial_skip_button;
    [SerializeField] private GameObject pre_wave_start_ui;
    [SerializeField] private GameObject bottom_ui_panel;
    [SerializeField] private GameObject middle_ui_panel;
    [SerializeField] private GameObject top_ui_panel;

    private static GameData game_data;


    public static void StartTutorial()
    {
        ExplainObjects();
    }


    public static void Skip()
    {
        Common.SafeDestroy(instance.tutorial_dialogue_controller.gameObject);
        Common.SafeDestroy(instance.gary.gameObject);
        Common.SafeDestroy(instance.pointer);
        Common.SafeDestroy(instance.tutorial_skip_button);

        instance.main_ui.SetActive(false);
        instance.pre_wave_start_ui.SetActive(false);

        instance.bottom_ui_panel.SetActive(true);
        instance.middle_ui_panel.SetActive(true);
        instance.top_ui_panel.SetActive(true);
    }


    public static void RedoTutorialWave()
    {
        instance.gary.gameObject.SetActive(true);
        instance.tutorial_skip_button.SetActive(false);
        instance.gary.MoveToTarget(new Vector3(16f, 115f, 0f), RedoTutorialDialogue);
    }


    private static void RedoTutorialDialogue()
    {
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_lost_dialogue,
                                                        ResetupTutorialWave);
    }


    private static void ResetupTutorialWave()
    {
        instance.pointer.SetActive(false);
        instance.gary.MoveToTarget(new Vector3(16f, 150f, 0f), LevelManager.ResetForTutorialWaveRetry);
    }


    private static void PositionPointerAndText(bool pointer_active,
                                               float pointer_x,
                                               float pointer_y,
                                               float text_x,
                                               float text_y)
    {
        instance.pointer.SetActive(pointer_active);
        instance.pointer.GetComponent<RectTransform>().localPosition = new Vector3(pointer_x, pointer_y, 0f);
        instance.tutorial_dialogue_controller.GetComponent<RectTransform>().localPosition = new Vector3(text_x, text_y, 0f);
    }


    private static void ExplainObjects()
    {
        instance.main_ui.SetActive(true);
        instance.tutorial_skip_button.SetActive(true);
        instance.bottom_ui_panel.SetActive(false);
        instance.middle_ui_panel.SetActive(false);
        instance.top_ui_panel.SetActive(true);
        instance.pre_wave_start_ui.SetActive(false);

        PositionPointerAndText(true, 75f, 80f, 0f, 80f);
        instance.underworld_manager.IdleUp();
        instance.tutorial_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[0],
                                                            ExplainObjects2,
                                                            false);
    }


    private static void ExplainObjects2()
    {
        instance.main_ui.SetActive(true);
        instance.tutorial_skip_button.SetActive(true);
        instance.bottom_ui_panel.SetActive(false);
        instance.middle_ui_panel.SetActive(false);
        instance.top_ui_panel.SetActive(true);
        instance.pre_wave_start_ui.SetActive(false);
        
        instance.underworld_manager.IdleDown();
        instance.tutorial_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[1],
                                                            ExplainObjects3,
                                                            true);
    }


    private static void ExplainObjects3()
    {
        instance.main_ui.SetActive(true);
        instance.tutorial_skip_button.SetActive(true);
        instance.bottom_ui_panel.SetActive(false);
        instance.middle_ui_panel.SetActive(false);
        instance.top_ui_panel.SetActive(true);
        instance.pre_wave_start_ui.SetActive(false);
        
        PositionPointerAndText(true, -130f, 100f, 0f, 80f);
        instance.underworld_manager.IdleLeft();
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[2],
                                                        ExplainObjects4,
                                                        false);
    }


    private static void ExplainObjects4()
    {
        PositionPointerAndText(true, -20f, -110f, 0f, -120f);
        instance.underworld_manager.IdleDown();
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[3],
                                                        ExplainLevelInfo);
    }


    private static void ExplainLevelInfo()
    {
        instance.tutorial_skip_button.SetActive(false);
        instance.bottom_ui_panel.SetActive(true);
        instance.middle_ui_panel.SetActive(false);
        instance.top_ui_panel.SetActive(true);
        instance.pre_wave_start_ui.SetActive(false);

        PositionPointerAndText(true, 75f, -120f, 0f, -120f);
        instance.underworld_manager.IdleDown();
        instance.tutorial_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[4],
                                                            MoveToShowCharacterInfo);
    }


    private static void MoveToShowCharacterInfo()
    {
        instance.pointer.SetActive(false);
        instance.underworld_manager.MoveToTarget(new Vector3(110f, 37f, 0f), ShowCharacterInfo);
    }


    private static void ShowCharacterInfo()
    {
        instance.pointer.SetActive(true);
        instance.main_ui.SetActive(true);
        instance.tutorial_skip_button.SetActive(true);
        instance.bottom_ui_panel.SetActive(false);
        instance.middle_ui_panel.SetActive(true);
        instance.top_ui_panel.SetActive(true);

        PositionPointerAndText(true, 75f, -37f, 0f, 20f);
        instance.underworld_manager.IdleRight();
        instance.tutorial_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[5],
                                                            MoveBackToPositionForQAIntro);
    }


    private static void MoveBackToPositionForQAIntro()
    {
        instance.pointer.SetActive(false);
        instance.middle_ui_panel.SetActive(false);
        instance.underworld_manager.MoveToTarget(new Vector3(176f, 37f, 0f), IntroTheQATester);
    }


    private static void IntroTheQATester()
    {
        instance.gary.gameObject.SetActive(true);
        instance.pointer.SetActive(false);
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[6],
                                                        StartQAWaveTransition);
    }


    private static void StartQAWaveTransition()
    {
        instance.tutorial_skip_button.SetActive(false);
        instance.underworld_manager.MoveToTarget(new Vector3(225f, 37f, 0f), null);
        instance.gary.MoveToTarget(new Vector3(16f, 115f, 0f), StartGaryDialogue);
    }


    private static void StartGaryDialogue()
    {
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[7],
                                                        ContinueGaryDialogue);
    }


    private static void ContinueGaryDialogue()
    {
        instance.pointer.SetActive(true);
        instance.main_ui.SetActive(true);
        instance.tutorial_skip_button.SetActive(false);
        instance.bottom_ui_panel.SetActive(true);
        instance.pre_wave_start_ui.SetActive(true);
        instance.middle_ui_panel.SetActive(false);
        instance.top_ui_panel.SetActive(true);

        PositionPointerAndText(true, 75f, -37f, 0f, 20f);
        instance.main_dialogue_controller.StartDialogue(game_data.tutorial_dialogues[8],
                                                        MoveGaryBack);
    }


    private static void MoveGaryBack()
    {
        instance.pointer.SetActive(false);
        instance.gary.MoveToTarget(new Vector3(16f, 150f, 0f), StartQAWave);
    }


    private static void StartQAWave()
    {
        instance.gary.gameObject.SetActive(false);
        LevelManager.ManagerHasExitedPreLevel();
    }



    // Start is called before the first frame update
    private void Start()
    {
        if(HandleSingleton())
        {
            game_data = GameData.Get();
        }
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second TutorialController created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
