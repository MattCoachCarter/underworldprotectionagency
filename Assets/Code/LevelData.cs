using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class LevelData
{
    public int starting_grid;
    public int max_available_grid;
    public bool is_tutorial = false;
    public DialogueData pre_level_dialogue;
    public List<WaveData> waves;
    public List<int> unlocks;
    public DialogueData post_level_dialogue;
}
