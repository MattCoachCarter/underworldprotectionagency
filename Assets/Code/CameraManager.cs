using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public enum CamManState {none, panning};


    public static CameraManager instance = null;

    private static readonly float[] GRID_Y_POSITIONS = new float[] {56f, 248f, 440f};
    private static readonly float MOVE_TIMING = 1.5f;

    private float start_y = 0f;
    private float target_y = 0f;
    private CamManState state = CamManState.none;
    private Action callback = null;
    private float timer = 0f;


    public static bool IsPanning()
    {
        return instance.state == CamManState.panning;
    }


    // Start is called before the first frame update
    private void Start()
    {
        HandleSingleton();
    }

    // Update is called once per frame
    private void Update()
    {
        if(state == CamManState.panning)
        {
            HandlePanning();
        }
    }


    public static void PanToGrid(int grid_index, Action cb)
    {
        instance.DoPanToGrid(grid_index, cb);
    }


    private void DoPanToGrid(int grid_index, Action cb)
    {
        timer = 0f;
        callback = cb;
        target_y = GRID_Y_POSITIONS[grid_index];
        start_y = transform.position.y;
        state = CamManState.panning;
    }


    private void HandlePanning()
    {
        timer += Time.deltaTime;
        float pct = timer / MOVE_TIMING;
        float current_y_position = Mathf.Lerp(start_y, target_y, pct);

        transform.position = new Vector3(transform.position.x,
                                         current_y_position,
                                         transform.position.z);

        if(timer >= MOVE_TIMING)
        {
            state = CamManState.none;
            if(callback != null)
            {
                callback();
            }
        }
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second CameraManager created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
