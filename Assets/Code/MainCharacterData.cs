using System;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class MainCharacterData
{
    public string first_name;
    public string last_name;
    public string animation_key;
    public MainCharacterStats stats;
    public List<string> talents;


    public MainCharacterData()
    {
        
    }

}
