using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Store : MonoBehaviour
{
    public static Store instance = null;

    [SerializeField] private Text object_name;
    [SerializeField] private Image object_image;
    [SerializeField] private Image object_button_image;
    [SerializeField] private Text object_description;
    [SerializeField] private Text object_price;
    [SerializeField] private Sprite button_background_selected;
    [SerializeField] private Sprite button_background_not_selected;
    [SerializeField] private GameObject lock_icon_game_object;
    [SerializeField] private GameObject object_image_game_object;

    private bool object_selected = false;
    private List<StoreObjectData> all_objects;
    private int current_object_index = 0;


    public void NavLeft()
    {
        current_object_index--;
        if(current_object_index < 0)
        {
            current_object_index = (all_objects.Count - 1);
        }
        ShowCurrentObjectInfo();
        if(object_selected)
        {
            Select();
        }
    }


    public void NavRight()
    {
        current_object_index++;
        if(current_object_index > (all_objects.Count - 1))
        {
            current_object_index = 0;
        }
        ShowCurrentObjectInfo();
        if(object_selected)
        {
            Select();
        }
    }


    public void Toggle()
    {
        if(object_selected)
        {
            Unselect();
        }
        else
        {
            Select();
        }

        SetButtonSprite();
    }


    public static StoreObjectData GetCurrentObjectData()
    {
        return instance.all_objects[instance.current_object_index];
    }


    public static StoreObjectData GetSelectedObjectData()
    {
        if(instance.object_selected)
        {
            return GetCurrentObjectData();
        }
        return null;
    }


    public static void UnlockObjects(List<int> object_ids)
    {
        foreach(int object_id in object_ids)
        {
            UnlockObject(object_id);
        }
    }


    public static void UnlockObject(int object_id)
    {
        foreach(StoreObjectData obj in instance.all_objects)
        {
            if(obj.id == object_id)
            {
                obj.locked = false;
                break;
            }
        }
    }


    private void Unselect()
    {
        object_selected = false;
        PlayerControlContext.UnselectPrefab();
        SetButtonSprite();
    }


    private void Select()
    {
        StoreObjectData data = GetCurrentObjectData();
        if(data.price <= LevelManager.GetGold() && !data.locked)
        {
            object_selected = true;
            PlayerControlContext.SelectPrefab(data.prefab_name);
            SetButtonSprite();
        }
        else
        {
            Unselect();
        }
    }


    // Start is called before the first frame update
    private void Start()
    {
        if(HandleSingleton())
        {
            InitializeStoreObjects();
            ShowCurrentObjectInfo();
        }
    }


    private void Update()
    {
        CheckPrice();
    }


    private void CheckPrice()
    {
        StoreObjectData data = GetCurrentObjectData();
        if(LevelManager.GetGold() >= data.price && !data.locked)
        {
            object_price.color = new Color(1f, 1f, 1f);
        }
        else
        {
            object_price.color = new Color(1f, 0f, 0f);
            if(object_selected)
            {
                Toggle();
            }
        }
    }


    private void SetButtonSprite()
    {
        if(object_selected)
        {
            object_button_image.sprite = button_background_selected;
        }
        else
        {
            object_button_image.sprite = button_background_not_selected;
        }
    }


    private void ShowCurrentObjectInfo()
    {
        StoreObjectData data = GetCurrentObjectData();
        object_name.text = data.name;
        object_image.sprite = Common.LoadSprite(data.sprite_name);
        object_description.text = data.description;
        object_price.text = data.price.ToString();

        if(data.locked)
        {
            lock_icon_game_object.SetActive(true);
            object_image_game_object.SetActive(false);
            object_price.text = "locked";
        }
        else
        {
            lock_icon_game_object.SetActive(false);
            object_image_game_object.SetActive(true);
        }
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second Store created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }


    private void InitializeStoreObjects()
    {
        all_objects = GameData.Get().buyables;
    }
}
