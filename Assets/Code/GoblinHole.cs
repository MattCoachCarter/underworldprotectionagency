using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinHole : PlayerObject
{
    private static readonly float GOBLIN_COOLDOWN = 10f;

    private float cooldown_timer = GOBLIN_COOLDOWN;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(IsArmed())
        {
            cooldown_timer -= Time.deltaTime;
            if(cooldown_timer <= 0f)
            {
                cooldown_timer = GOBLIN_COOLDOWN;
                SpawnGoblin();
            }
        }
    }


    private void SpawnGoblin()
    {
        Debug.Log("Spawn goblin");
        // TODO
    }
}
