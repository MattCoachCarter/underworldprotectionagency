using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarlockShot : Hurtbox
{
    private static readonly float MOVE_SPD = 30f;

    private SpriteRenderer sprite_renderer;
    private SpriteAnimator sprite_animator;
    private float move_x = 0f;
    private float move_y = 0f;
    private PlayerObject parent;


    protected override void Awake()
    {
        base.Awake();
        sprite_renderer = GetComponentInChildren<SpriteRenderer>();
        sprite_animator = GetComponentInChildren<SpriteAnimator>();
        SetUpAnimations();
    }


    public void HitPlayer()
    {
        Common.SafeDestroy(gameObject);
        LevelManager.GetCurrentMainCharacter().AddDebuff(Debuff.amplify);
    }


    public void Setup(bool _player_controlled, Vector3 move_target, PlayerObject p)
    {
        parent = p;
        SetupHurtbox(_player_controlled, HitPlayer);
        if(move_target.x > transform.position.x)
        {
            move_x = 1f;
        }
        else if(move_target.x < transform.position.x)
        {
            move_x = -1f;
        }
        if(move_target.y > transform.position.y)
        {
            move_y = 1f;
        }
        else if(move_target.y < transform.position.y)
        {
            move_y = -1f;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        HandleMoving();
    }


    private void HandleMoving()
    {
        float movement_this_frame = Time.deltaTime * MOVE_SPD;
        Vector3 move_vector = new Vector3(move_x, move_y, 0) * movement_this_frame;
        transform.position += move_vector;
    }


    private bool ShouldDestroy(Space space)
    {
        return (space.IsBlocked() && space.GetPlayerObject() != parent);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "space")
        {
            Space s = col.gameObject.GetComponent<Space>();
            if(s != null && ShouldDestroy(s))
            {
                Debug.Log("Destroying due to space collision");
                Common.SafeDestroy(gameObject);
            }
        }
        else if(col.gameObject.tag == "bound")
        {
            Debug.Log("Destroying due to bounds collision");
            Common.SafeDestroy(gameObject);
        }
    }


    private void SetUpAnimations()
    {
        sprite_animator.Setup(GetAnimations(), "default");
    }


    private void PlayAnimation(string animation_name)
    {
        sprite_animator.PlayAnimation(animation_name);
    }


    private void LoopAnimation(string animation_name)
    {
        sprite_animator.SetDefaultAnimation(animation_name);
        sprite_animator.PlayAnimation(animation_name);
    }


    private List<AnimationSpriteSet> GetAnimations()
    {
        string animation_directory = "Spritesheets/warlock_shot/";
        List<AnimationSpriteSet> animations = new List<AnimationSpriteSet>();
        animations.Add(new AnimationSpriteSet("default",
                                              animation_directory +"warlock_shot",
                                              0.75f));
        return animations;
    }
}
