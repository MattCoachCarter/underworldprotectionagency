using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlContext : MonoBehaviour
{
    public static PlayerControlContext instance = null;

    public static string pending_placement_prefab = null;
    public static GameObject pending_placement = null;


    public static void SelectSpace(Space space)
    {
        if(IsSpaceAllowed(space))
        {
            if(pending_placement == null)
            {
                ShowSpaceDetails(space);
            }
            else
            {
                PlacePlayerObject(space);
            }
        }
    }


    public static bool IsSpaceAllowed(Space space)
    {
        if(pending_placement == null)
        {
            return true;
        }
        
        return space.IsEmpty() && !WouldBlockPath(space) && !IsPlayerMovingToSpace(space);
    }


    public static bool PrefabIsSelected()
    {
        return pending_placement_prefab != null;
    }


    public static void UnselectPrefab()
    {
        Common.SafeDestroy(pending_placement);
        pending_placement_prefab = null;
    }


    public static void SelectPrefab(string prefab_string)
    {
        pending_placement_prefab = prefab_string;
        Common.SafeDestroy(pending_placement);
        SpawnPendingPlacement();
    }


    private static void SpawnPendingPlacement()
    {
        pending_placement = Common.Spawn(pending_placement_prefab, new Vector3(200, 200, 200));
    }


    // Start is called before the first frame update
    private void Start()
    {
        if(HandleSingleton())
        {

        }
    }


    // Update is called once per frame
    private void Update()
    {
        
    }


    private static void PlacePlayerObject(Space space)
    {
        LevelManager.DeductGold(Store.GetSelectedObjectData().price);
        space.SetPlayerObject(pending_placement);
        SpawnPendingPlacement();
    }


    private static bool IsPlayerMovingToSpace(Space space)
    {
        try
        {
            return LevelManager.GetCurrentMainCharacter().GetTargetSpace() == space;
        }
        catch(NullReferenceException) {}
        return false;
    }


    private static bool WouldBlockPath(Space space)
    {
        if(pending_placement == null)
        {
            return false;
        }

        PlayerObject player_object = pending_placement.GetComponent<PlayerObject>();
        if(!player_object.IsBlocking())
        {
            return false;
        }

        GameGrid grid = LevelManager.GetCurrentGrid();
        MainCharacter character = LevelManager.GetCurrentMainCharacter();

        // TODO: Also make sure we can reach exit from where main character is
        bool can_reach_exit_from_entrance = CanReachExitFrom(grid,
                                                             grid.GetEntranceX(),
                                                             grid.GetEntranceY(),
                                                             space);

        bool can_reach_exit_from_character = true;
        try
        {
            if(character != null)
            {
                can_reach_exit_from_character = CanReachExitFrom(grid,
                                                                 character.GetX(),
                                                                 character.GetY(),
                                                                 space);
            }
        }
        catch(NullReferenceException) {}

        return !can_reach_exit_from_entrance || !can_reach_exit_from_character;
    }


    private static bool CanReachExitFrom(GameGrid grid, int x, int y, Space extra_blocked_space)
    {
        return CanReachExitFrom(grid, x, y, extra_blocked_space, new List<Tuple<int, int>>());
    }


    private static bool CanReachExitFrom(GameGrid grid,
                                         int x,
                                         int y,
                                         Space extra_blocked_space,
                                         List<Tuple<int, int>> visited_locations)
    {
        if(x == grid.GetExitX() && y == 0)
        {
            return true;
        }

        List<Tuple<int, int>> accessible_neighbors = new List<Tuple<int, int>>();

        if((x - 1) >= 0 &&
           !grid.GetSpace((x - 1), y).IsBlocked() &&
           !extra_blocked_space.IsAtCoords((x - 1), y))
        {
            Tuple<int, int> neighbor_coords = new Tuple<int, int>((x - 1), y);
            if(!visited_locations.Contains(neighbor_coords))
            {
                accessible_neighbors.Add(neighbor_coords);
                visited_locations.Add(neighbor_coords);
            }
        }
        if((x + 1) < GameConfig.GRID_WIDTH &&
           !grid.GetSpace((x + 1), y).IsBlocked() &&
           !extra_blocked_space.IsAtCoords((x + 1), y))
        {
            Tuple<int, int> neighbor_coords = new Tuple<int, int>((x + 1), y);
            if(!visited_locations.Contains(neighbor_coords))
            {
                accessible_neighbors.Add(neighbor_coords);
                visited_locations.Add(neighbor_coords);
            }
        }
        if((y - 1) >= 0 &&
           !grid.GetSpace(x, y - 1).IsBlocked() &&
           !extra_blocked_space.IsAtCoords(x, (y - 1)))
        {
            Tuple<int, int> neighbor_coords = new Tuple<int, int>(x, (y - 1));
            if(!visited_locations.Contains(neighbor_coords))
            {
                accessible_neighbors.Add(neighbor_coords);
                visited_locations.Add(neighbor_coords);
            }
        }
        if((y + 1) < GameConfig.GRID_HEIGHT &&
           !grid.GetSpace(x, y + 1).IsBlocked() &&
           !extra_blocked_space.IsAtCoords(x, (y + 1)))
        {
            Tuple<int, int> neighbor_coords = new Tuple<int, int>(x, (y + 1));
            if(!visited_locations.Contains(neighbor_coords))
            {
                accessible_neighbors.Add(neighbor_coords);
                visited_locations.Add(neighbor_coords);
            }
        }

        foreach(Tuple<int, int> neighbor_coords in accessible_neighbors)
        {
            if(CanReachExitFrom(grid,
                                neighbor_coords.Item1,
                                neighbor_coords.Item2,
                                extra_blocked_space,
                                visited_locations))
            {
                return true;
            }
        }

        return false;
    }


    private static void ShowSpaceDetails(Space space)
    {
        // TODO
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second PlayerControlContext created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
