using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Functions
public static class Common
{
    // CACHES:
    public static Dictionary<string, GameObject> resource_cache = new Dictionary<string, GameObject>();
    public static Dictionary<string, Sprite> sprite_cache = new Dictionary<string, Sprite>();
    public static Dictionary<string, Sprite[]> spritesheet_cache = new Dictionary<string, Sprite[]>();

    public static bool PctChance(int chance) { return PctChance((float)chance); }
    public static bool PctChance(float chance)
    {
        return (chance > UnityEngine.Random.Range(0.0f, 100f));
    }

    public static GameObject Spawn(string name, float x, float y, float z)
    {
        return InstantiateObject(name, x, y, z, true);
    }

    /**
     * Spawn the game object denoted by the given name at the given position
     */
    public static GameObject Spawn(string name, Vector3 pos)
    {
        return InstantiateObject(name, pos.x, pos.y, pos.z, true);
    }

    /**
     * Spawn the game object denoted by the given name, at 0,0,0
     */
    public static GameObject Spawn(string name)
    {
        return InstantiateObject(name, 0f, 0f, 0f, true);
    }

    /**
     * Spawn the game object denoted by the given name, at 0,0, with no rotation and no z
     */
    public static GameObject SpawnNoCache(string name)
    {
        return InstantiateObject(name, 0f, 0f, 0f, false);
    }

    /**
     * Do an actual object instantiation
     */
    public static GameObject InstantiateObject(string to_instantiate, float x, float y, float z, bool do_caching)
    {
        GameObject instantiated = null;

        // First load in the resource:
        GameObject go_to_instantiate = LoadResource(to_instantiate, do_caching);

        // If we were able to load a resource, instantiate the object
        if(go_to_instantiate == null)
        {
            Debug.LogWarning("Could not instantiate game object with name: "+ to_instantiate);
        }
        else
        {
            instantiated = GameObject.Instantiate(go_to_instantiate, new Vector3(x, y, z), Quaternion.identity) as GameObject;
        }

        return instantiated;
    }


    /**
     * Cache the prefab by the given name
     */
    public static void CachePrefab(string to_cache)
    {
        LoadResource(to_cache, true);
    }



    /**
     * Load in the given resource
     */
    public static GameObject LoadResource(string to_load, bool do_caching)
    {
        if(!do_caching)
        {
            return (GameObject)Resources.Load(to_load);
        }

        GameObject resource = null;
        bool cached = resource_cache.ContainsKey(to_load);

        if(cached)
        {
            resource = resource_cache[to_load];
        }
        else
        {
            try
            {
                Debug.Log("Loading resource: "+ to_load);
                resource = (GameObject)Resources.Load(to_load);
            }
            catch
            {
                Debug.LogError("Could not load resource: "+ to_load);
            }
            if(resource == null)
            {
                Debug.LogError("Could not load resource: "+ to_load);
            }
            resource_cache.Add(to_load, resource);
        }

        return resource;
    }


    /**
     * Safely destroy the given game object, check to see if it is null first.
     * Return true if we really destry the object, false otherwise
     */
    public static bool SafeDestroy(GameObject thing_to_destroy)
    {
        bool destroyed = false;

        if(thing_to_destroy != null)
        {
            GameObject.Destroy(thing_to_destroy);
            destroyed = true;
        }

        return destroyed;
    }


    public static GameManager GetGameManager()
    {
        return GameManager.instance;
    }


    public static GameConfig GetGameConfig()
    {
        return GameConfig.Instance;
    }


    public static GameObject GetCameraGameObject()
    {
        // TODO: This is by no means fool/full proof
        return GameObject.Find("Main Camera");
    }


    public static Camera GetCamera()
    {
        return GetCameraGameObject().GetComponent<Camera>();
    }


    public static Vector3 WorldToScreenPosition(Vector3 world_position)
    {
        float world_left_bound = -24f;
        float world_right_bound = 216f;
        float y_offset = (float)(LevelManager.GetCurrentGridIndex() * 192);
        float world_top_bound = 136f + y_offset;
        float world_bottom_bound = -24f + y_offset;

        float x_pct = Mathf.InverseLerp(world_left_bound, world_right_bound, world_position.x);
        float y_pct = Mathf.InverseLerp(world_bottom_bound, world_top_bound, world_position.y);

        float canvas_x = Mathf.Lerp(-240f, 240f, x_pct);
        float canvas_y = Mathf.Lerp(-160f, 160f, y_pct);
        Vector3 pos = new Vector3(canvas_x,
                                  canvas_y,
                                  0);
        
        Debug.Log("World pos: "+ world_position);
        Debug.Log("Screen pos: "+ pos);
        return pos;
    }


    public static Vector3 ScreenToWorldPosition(Vector3 screen_position)
    {
        Camera camera_handle = GetCamera();
        return camera_handle.ScreenToWorldPoint(screen_position);
    }


    public static string CapitalizeStr(string input_str)
    {
        if(input_str.Length == 1)
        {
            return (string)char.ToUpper(input_str[0]).ToString();
        }
        else if(input_str.Length > 1)
        {
            return char.ToUpper(input_str[0]) + input_str.Substring(1);
        }

        return "";
    }


    public static int ApproachTarget(int val, int approach, int target)
    {
        if(val > target)
        {
            val -= approach;
            if(val < target)
            {
                return target;
            }
            return val;
        }
        else if(val < target)
        {
            val += approach;
            if(val > target)
            {
                return target;
            }
            return val;
        }

        return target;
    }


    /**
     * Load in the given sprite
     */
    public static Sprite LoadSprite(string to_load)
    {
        Sprite sprite = null;
        bool cached = sprite_cache.ContainsKey(to_load);

        if(cached)
        {
            sprite = sprite_cache[to_load];
        }
        else
        {
            try
            {
                sprite = Resources.Load<Sprite>(to_load);
                Debug.Log("Loading and caching sprite: "+ to_load);
            }
            catch
            {
                Debug.LogError("Could not load sprite: "+ to_load);
            }
            if(sprite == null)
            {
                Debug.LogError("Could not load sprite: "+ to_load);
            }
            sprite_cache.Add(to_load, sprite);
        }

        return sprite;
    }
    

    /**
     * Load in the given sprite
     */
    public static Sprite[] LoadSpritesheet(string to_load)
    {
        Sprite[] sprites = null;
        bool cached = spritesheet_cache.ContainsKey(to_load);

        if(cached)
        {
            sprites = spritesheet_cache[to_load];
        }
        else
        {
            Debug.Log("Loading spritesheet: "+ to_load);
            try
            {
                sprites = Resources.LoadAll<Sprite>(to_load);
            }
            catch
            {
                Debug.LogError("Could not load spritesheet: "+ to_load);
            }
            if(sprites == null)
            {
                Debug.LogError("Could not load spritesheet: "+ to_load);
            }
            spritesheet_cache.Add(to_load, sprites);
        }

        return sprites;
    }


    public static float ApproachTarget(float val, float approach, float target)
    {
        if(val > target)
        {
            val -= approach;
            if(val < target)
            {
                return target;
            }
            return val;
        }
        else if(val < target)
        {
            val += approach;
            if(val > target)
            {
                return target;
            }
            return val;
        }

        return target;
    }


    public static int ApproachZero(int val, int approach)
    {
        return ApproachTarget(val, approach, 0);
    }


    public static float ApproachZero(float val, float approach)
    {
        return ApproachTarget(val, approach, 0f);
    }


    public static Vector3 GetMousePosition()
    {
        return Input.mousePosition;
    }


    public static Vector3 GetMouseWorldPosition()
    {
        return ScreenToWorldPosition(GetMousePosition());
    }


    public static Material LoadMaterial(string material_name)
    {
        return Resources.Load(material_name, typeof(Material)) as Material;
    }


    public static bool IsRoughlyOne(float num)
    {
        return (num == 1f || (num > 0.999 && num < 1.001));
    }


    public static bool IsRoughlyZero(float num)
    {
        return (num == 0f || (num > -0.009 && num < 0.001));
    }


    public static float NormalizeEulerRotation(float r)
    {
        if(r > 180f)
        {
            r -= 360f;
        }
        if(r < -180f)
        {
            r += 360f;
        }
        return r;
    }


    /**
     * Given a game object, disable all the colliders on that game object
     */
    public static void DisableAllColliders(GameObject game_object)
    {
        foreach(Collider c in game_object.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }
    }
}
