using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Hurtbox
{
    private static readonly float FIREBALL_TTL = 1.25f;
    private static readonly float FIREBALL_MOVE_SPD = 30f;

    private float time_alive = 0f;
    private SpriteRenderer sprite_renderer;
    private Vector3 move_target;


    public void Setup(bool _player_controlled, Vector3 _move_target)
    {
        SetupHurtbox(_player_controlled);
        move_target = new Vector3(_move_target.x,
                                  _move_target.y,
                                  transform.position.z);
    }


    // Start is called before the first frame update
    void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
        time_alive = 0f;
    }


    // Update is called once per frame
    void Update()
    {
        MoveToTarget();

        time_alive += Time.deltaTime;
        if(time_alive >= FIREBALL_TTL)
        {
            Common.SafeDestroy(gameObject);
        }
        else
        {
            AdjustAlpha();
        }
    }


    private void MoveToTarget()
    {
        float movement_this_frame = Time.deltaTime * FIREBALL_MOVE_SPD;
        transform.position = Vector3.MoveTowards(transform.position,
                                                 move_target,
                                                 movement_this_frame);
    }


    private void AdjustAlpha()
    {
        float half_ttl = (FIREBALL_TTL / 2f);
        float pct_adjustment = time_alive / half_ttl;
        float min = 0.25f;
        float max = 0.75f;
        if(time_alive >= half_ttl)
        {
            min = 0.75f;
            max = 0f;
            pct_adjustment = (time_alive / 2f) / half_ttl;
        }

        float new_alpha = Mathf.Lerp(min, max, pct_adjustment);
        sprite_renderer.color = new Color(1f, 1f, 1f, new_alpha);
    }
}
