using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrap : PlayerObject
{
    private static readonly float SHOOT_COOLDOWN = 2f;
    private static readonly string FIREBALL_PREFAB = "Prefabs/Fireball";

    private float shoot_timer = 0f;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(IsArmed())
        {
            HandleShooting();
        }
    }


    protected override void OnDisarm()
    {
        shoot_timer = 0f;
    }


    protected override void OnArm()
    {
        shoot_timer = 0f;
    }


    private void HandleShooting()
    {
        shoot_timer -= Time.deltaTime;
        if(shoot_timer <= 0f)
        {
            shoot_timer = SHOOT_COOLDOWN;
            Shoot();
        }
    }


    private void Shoot()
    {
        foreach(Vector3 shoot_target in GetShootTargets())
        {
            ShootAtTarget(shoot_target);
        }
    }


    private List<Vector3> GetShootTargets()
    {
        List<Vector3> shoot_targets = new List<Vector3>();
        foreach(Space unblocked_neighbor_space in GetUnblockedNeighbors())
        {
            shoot_targets.Add(unblocked_neighbor_space.transform.position);
        }
        return shoot_targets;
    }


    private void ShootAtTarget(Vector3 shoot_target)
    {
        Vector3 spawn_pos = new Vector3(transform.position.x,
                                        transform.position.y,
                                        GameConfig.HURTBOX_Z);
        GameObject fireball_game_object = Common.Spawn(FIREBALL_PREFAB, spawn_pos);
        fireball_game_object.GetComponent<Fireball>().Setup(true, shoot_target);
    }
}
