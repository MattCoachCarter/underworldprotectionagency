using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceTrap : PlayerObject
{
    private static readonly float DAMAGE_COOLDOWN = 1f;
    private static readonly string DAMAGE_PREFAB = "Prefabs/IceHurtbox";

    private float damage_timer = 0f;
    private GameObject hurtbox_game_object = null;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(IsArmed())
        {
            if(hurtbox_game_object == null)
            {
                damage_timer -= Time.deltaTime;
                if(damage_timer <= 0f)
                {
                    SpawnHurtbox();
                }
            }
            else
            {
                hurtbox_game_object.SetActive(true);
            }
        }
        else if(hurtbox_game_object != null)
        {
            hurtbox_game_object.SetActive(false);
        }
    }


    protected override void OnDisarm()
    {
        damage_timer = 0f;
    }


    protected override void OnArm()
    {
        damage_timer = 0f;
    }


    private void SpawnHurtbox()
    {
        damage_timer = DAMAGE_COOLDOWN;
        hurtbox_game_object = Common.Spawn(DAMAGE_PREFAB, transform.position);
        hurtbox_game_object.GetComponent<Hurtbox>().SetupHurtbox(true, HitPlayer);
    }


    private void HitPlayer()
    {
        Common.SafeDestroy(hurtbox_game_object);
        LevelManager.GetCurrentMainCharacter().AddDebuff(Debuff.frozen);
    }
}
