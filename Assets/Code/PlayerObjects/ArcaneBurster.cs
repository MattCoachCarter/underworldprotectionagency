using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneBurster : PlayerObject
{
    private static readonly float SHOOT_COOLDOWN = 8f;
    private static readonly string PROJECTILE_PREFAB = "Prefabs/ArcaneBurst";
    private SpriteRenderer sprite_renderer;
    private SpriteAnimator sprite_animator;
    private List<Space> radius;

    private float shoot_timer = 0f;


    private void Awake()
    {
        sprite_renderer = GetComponentInChildren<SpriteRenderer>();
        sprite_animator = GetComponentInChildren<SpriteAnimator>();
        SetUpAnimations();
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }


    public override void Setup(Space _space)
    {
        base.Setup(_space);
        PopulateRadius();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(IsArmed())
        {
            HandleShooting();
        }
    }


    protected override void OnDisarm()
    {
        shoot_timer = 0f;
    }


    protected override void OnArm()
    {
        shoot_timer = 0f;
    }


    private void HandleShooting()
    {
        shoot_timer -= Time.deltaTime;
        if(shoot_timer <= 0f)
        {
            if(PlayerInRadius())
            {
                shoot_timer = SHOOT_COOLDOWN;
                Shoot();
            }
        }
    }


    private bool PlayerInRadius()
    {
        return radius.Contains(LevelManager.GetCurrentMainCharacter().GetSpace());
    }


    private void Shoot()
    {
        foreach(Space s in radius)
        {
            ShootAtTarget(s.transform.position);
        }
    }


    private void PopulateRadius()
    {
        int home_x = space.GetX();
        int home_y = space.GetY();
        radius = new List<Space>();

        GameGrid grid = space.GetParentGrid();
        Space s;

        s = grid.GetSpace(home_x + 1, home_y + 1);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x + 1, home_y + 0);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x + 1, home_y - 1);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x - 1, home_y + 1);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x - 1, home_y + 0);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x - 1, home_y - 1);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x + 0, home_y + 1);
        if(s != null)
        {
            radius.Add(s);
        }

        s = grid.GetSpace(home_x + 0, home_y - 1);
        if(s != null)
        {
            radius.Add(s);
        }
    }


    private void ShootAtTarget(Vector3 shoot_target)
    {
        Vector3 spawn_pos = new Vector3(shoot_target.x,
                                        shoot_target.y,
                                        GameConfig.HURTBOX_Z);
        GameObject projectile_object = Common.Spawn(PROJECTILE_PREFAB, spawn_pos);
        projectile_object.GetComponent<ArcaneBurst>().Setup(true);
    }


    private void SetUpAnimations()
    {
        sprite_animator.Setup(GetAnimations(), "default");
    }


    private void PlayAnimation(string animation_name)
    {
        sprite_animator.PlayAnimation(animation_name);
    }


    private void LoopAnimation(string animation_name)
    {
        sprite_animator.SetDefaultAnimation(animation_name);
        sprite_animator.PlayAnimation(animation_name);
    }


    private List<AnimationSpriteSet> GetAnimations()
    {
        string animation_directory = "Spritesheets/arcane_burster/";
        List<AnimationSpriteSet> animations = new List<AnimationSpriteSet>();
        animations.Add(new AnimationSpriteSet("default",
                                              animation_directory +"arcane_burster",
                                              0.75f));
        return animations;
    }
}
