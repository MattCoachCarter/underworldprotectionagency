using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarlockAltar : PlayerObject
{
    private static readonly float SHOOT_COOLDOWN = 2f;
    private static readonly string PROJECTILE_PREFAB = "Prefabs/WarlockShot";
    private SpriteRenderer sprite_renderer;
    private SpriteAnimator sprite_animator;

    private float shoot_timer = 0f;


    private void Awake()
    {
        sprite_renderer = GetComponentInChildren<SpriteRenderer>();
        sprite_animator = GetComponentInChildren<SpriteAnimator>();
        SetUpAnimations();
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(IsArmed())
        {
            HandleShooting();
        }
    }


    protected override void OnDisarm()
    {
        shoot_timer = 0f;
    }


    protected override void OnArm()
    {
        shoot_timer = 0f;
    }


    private void HandleShooting()
    {
        shoot_timer -= Time.deltaTime;
        if(shoot_timer <= 0f)
        {
            shoot_timer = SHOOT_COOLDOWN;
            Shoot();
        }
    }


    private void Shoot()
    {
        foreach(Space neighbor in GetNeighbors())
        {
            ShootAtTarget(neighbor.transform.position);
        }
    }


    private void ShootAtTarget(Vector3 shoot_target)
    {
        Vector3 spawn_pos = new Vector3(transform.position.x,
                                        transform.position.y,
                                        GameConfig.HURTBOX_Z);
        GameObject projectile_object = Common.Spawn(PROJECTILE_PREFAB, spawn_pos);
        projectile_object.GetComponent<WarlockShot>().Setup(true, shoot_target, this);
    }


    private void SetUpAnimations()
    {
        sprite_animator.Setup(GetAnimations(), "default");
    }


    private void PlayAnimation(string animation_name)
    {
        sprite_animator.PlayAnimation(animation_name);
    }


    private void LoopAnimation(string animation_name)
    {
        sprite_animator.SetDefaultAnimation(animation_name);
        sprite_animator.PlayAnimation(animation_name);
    }


    private List<AnimationSpriteSet> GetAnimations()
    {
        string animation_directory = "Spritesheets/warlock_altar/";
        List<AnimationSpriteSet> animations = new List<AnimationSpriteSet>();
        animations.Add(new AnimationSpriteSet("default",
                                              animation_directory +"warlock_altar",
                                              0.75f));
        return animations;
    }
}
