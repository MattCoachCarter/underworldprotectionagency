using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : HoverableGameObject
{
    [SerializeField] private bool blocking = true;
    [SerializeField] private bool damageable = false;
    [SerializeField] private int health = 0;

    private bool armed = false;
    protected Space space;


    public virtual void Setup(Space _space)
    {
        space = _space;
        Arm();
    }


    public void Arm()
    {
        armed = true;
        OnArm();
    }


    public void Disarm()
    {
        armed = false;
        OnDisarm();
    }


    public bool IsArmed()
    {
        return armed;
    }


    public bool IsBlocking()
    {
        return blocking;
    }


    public bool IsDamageable()
    {
        return damageable;
    }


    public int GetHealth()
    {
        return health;
    }


    protected virtual void OnDisarm()
    {

    }


    protected virtual void OnArm()
    {

    }


    protected List<Space> GetNeighbors()
    {
        return LevelManager.GetCurrentGrid().GetNeighbors(space);
    }


    protected List<Space> GetUnblockedNeighbors()
    {
        return LevelManager.GetCurrentGrid().GetUnblockedNeighbors(space);
    }
}
