using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderworldManager : MonoBehaviour
{
    public enum MgrState {none, idle, moving};

    [SerializeField] private string animation_key;

    private static readonly float MOVE_SPEED = 45f;

    private Direction facing;
    private MgrState state;
    private SpriteRenderer sprite_renderer;
    private SpriteAnimator sprite_animator;
    private Vector3 move_target;
    private Action move_callback;


    // Start is called before the first frame update
    private void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
        sprite_animator = GetComponent<SpriteAnimator>();
        SetUpAnimations();
        SetStateAndFacing(MgrState.idle, Direction.down);
    }


    // Update is called once per frame
    private void FixedUpdate()
    {
        HandleMoving();
    }


    public void MoveToTarget(Vector3 new_move_target, Action callback)
    {
        move_callback = callback;
        move_target = new_move_target;
        if(move_target.x > transform.position.x)
        {
            facing = Direction.right;
        }
        else if(move_target.x < transform.position.x)
        {
            facing = Direction.left;
        }
        else if(move_target.y > transform.position.y)
        {
            facing = Direction.up;
        }
        else
        {
            facing = Direction.down;
        }

        SetStateAndFacing(MgrState.moving, facing);
    }


    private void HandleMoving()
    {
        if(state == MgrState.moving)
        {
            float move_x = 0f;
            float move_y = 0f;

            if(facing == Direction.down)
            {
                move_y = -1f;
            }
            else if(facing == Direction.up)
            {
                move_y = 1f;
            }
            if(facing == Direction.left)
            {
                move_x = -1f;
            }
            else if(facing == Direction.right)
            {
                move_x = 1f;
            }

            Vector3 move_vector = new Vector3(move_x, move_y, 0) * MOVE_SPEED * Time.fixedDeltaTime;
            transform.position += move_vector;

            if(GetDistanceToTarget() < (MOVE_SPEED / 20f))
            {
                ReachedMoveTarget();
            }
        }
    }


    private void ReachedMoveTarget()
    {
        transform.position = move_target;
        SetStateAndFacing(MgrState.idle, Direction.down);
        if(move_callback != null)
        {
            move_callback();
        }
    }


    private float GetDistanceToTarget()
    {
        return Vector3.Distance(new Vector3(transform.position.x,
                                            transform.position.y,
                                            0f),
                                new Vector3(move_target.x,
                                            move_target.y,
                                            0f));
    }


    public void IdleUp()
    {
        SetStateAndFacing(MgrState.idle, Direction.up);
    }


    public void IdleDown()
    {
        SetStateAndFacing(MgrState.idle, Direction.down);
    }


    public void IdleLeft()
    {
        SetStateAndFacing(MgrState.idle, Direction.left);
    }


    public void IdleRight()
    {
        SetStateAndFacing(MgrState.idle, Direction.right);
    }


    private void SetStateAndFacing(MgrState new_state, Direction new_facing)
    {
        state = new_state;
        facing = new_facing;
        SetAnimationByStateAndFacing();
    }


    private void SetAnimationByStateAndFacing()
    {
        string animation_direction = "down";
        if(facing == Direction.up)
        {
            animation_direction = "up";
        }
        else if(facing != Direction.down)
        {
            animation_direction = "side";
        }

        string animation_name = "idle";
        switch(state)
        {
            case MgrState.moving:
                animation_name = "walk";
                break;
            default:
                animation_name = "idle";
                break;
        }

        bool sprite_flip = false;
        if(facing == Direction.left)
        {
            sprite_flip = true;
        }

        string animation_to_play = animation_name +"_"+ animation_direction;
        sprite_renderer.flipX = sprite_flip;
        LoopAnimation(animation_to_play);
    }


    private void SetUpAnimations()
    {
        sprite_animator.Setup(GetAnimations(), "idle_down");
    }


    private void PlayAnimation(string animation_name)
    {
        sprite_animator.PlayAnimation(animation_name);
    }


    private void LoopAnimation(string animation_name)
    {
        sprite_animator.SetDefaultAnimation(animation_name);
        sprite_animator.PlayAnimation(animation_name);
    }


    private List<AnimationSpriteSet> GetAnimations()
    {
        string animation_directory = "Spritesheets/"+ animation_key +"/";
        List<AnimationSpriteSet> animations = new List<AnimationSpriteSet>();
        animations.Add(new AnimationSpriteSet("idle_down",
                                              animation_directory +"idle_down",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("idle_up",
                                              animation_directory +"idle_up",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("idle_side",
                                              animation_directory +"idle_side",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("walk_down",
                                              animation_directory +"walk_down",
                                              0.25f));
        animations.Add(new AnimationSpriteSet("walk_up",
                                              animation_directory +"walk_up",
                                              0.25f));
        animations.Add(new AnimationSpriteSet("walk_side",
                                              animation_directory +"walk_side",
                                              0.25f));

        return animations;
    }
}
