using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuHandler : MonoBehaviour
{
    [SerializeField] private GameObject options_sub_menu;
    [SerializeField] private GameObject resolution_sub_menu;
    [SerializeField] private GameObject about_sub_menu;


    private void Awake()
    {
        SetResolution(960, 640);
    }


    public void StartGame()
    {
        BlackOverlay.FadeIn(GameManager.LoadLevelScene);
    }


    public void GoToAbout()
    {
        about_sub_menu.SetActive(true);
    }


    public void BackFromAbout()
    {
        about_sub_menu.SetActive(false);
    }


    public void GoToOptions()
    {
        if(Application.platform != RuntimePlatform.WebGLPlayer)
        {
            options_sub_menu.SetActive(true);
        }
    }


    public void GoToResolutionsMenu()
    {
        if(Application.platform != RuntimePlatform.WebGLPlayer)
        {
            resolution_sub_menu.SetActive(true);
        }
    }


    public void BackFromResolutionsMenu()
    {
        resolution_sub_menu.SetActive(false);
    }


    public void BackFromOptions()
    {
        options_sub_menu.SetActive(false);
    }


    public void Exit()
    {
        Application.Quit();
    }


    public void SetResolution480320()
    {
        SetResolution(480, 320);
    }


    public void SetResolution960640()
    {
        SetResolution(960, 640);
    }


    public void SetResolution1440960()
    {
        SetResolution(1440, 960);
    }


    public void SetResolution19201280()
    {
        SetResolution(1920, 1280);
    }


    public void SetResolution(int x, int y)
    {
        if(Application.platform != RuntimePlatform.WebGLPlayer)
        {
            GameConfig.X_RES = x;
            GameConfig.Y_RES = y;
            Screen.SetResolution(x, y, false);
        }
    }
}
