using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BlackOverlay : MonoBehaviour
{
    public enum FadeMode {none, fade_in, fade_out, fade_in_and_out};
    public enum FadeState {none, fading_in, fading_out};

    public static BlackOverlay instance = null;

    private static readonly float FADE_TIMING = 0.6f;

    [SerializeField] private Image img;

    private Action callback = null;
    private FadeMode fade_mode = FadeMode.none;
    private FadeState state = FadeState.none;
    private float timer = 0f;


    public static void FadeIn() { FadeIn(null); }
    public static void FadeIn(Action cb)
    {
        instance.timer = 0f;
        instance.fade_mode = FadeMode.fade_in;
        instance.callback = cb;
        instance.state = FadeState.fading_in;
        instance.gameObject.SetActive(true);
    }


    public static void FadeOut() { FadeOut(null); }
    public static void FadeOut(Action cb)
    {
        instance.timer = 0f;
        instance.fade_mode = FadeMode.fade_out;
        instance.callback = cb;
        instance.state = FadeState.fading_out;
        instance.gameObject.SetActive(true);
    }


    public static void FadeInAndOut() { FadeInAndOut(null); }
    public static void FadeInAndOut(Action cb)
    {
        instance.timer = 0f;
        instance.fade_mode = FadeMode.fade_in_and_out;
        instance.callback = cb;
        instance.state = FadeState.fading_in;
        instance.gameObject.SetActive(true);
    }


    // Start is called before the first frame update
    void Start()
    {
        if(HandleSingleton())
        {
        }
    }


    // Update is called once per frame
    void Update()
    {
        switch(state)
        {
            case FadeState.fading_in:
                IncrementTimer();
                HandleFadingIn();
                break;
            case FadeState.fading_out:
                IncrementTimer();
                HandleFadingOut();
                break;
            default:
                gameObject.SetActive(false);
                break;
        }
    }


    private void IncrementTimer()
    {
        timer += Time.deltaTime;
    }


    private void HandleFadingIn()
    {
        ComputeAndSetAlpha(0f, 1f);
        if(IsDoneFading())
        {
            if(fade_mode == FadeMode.fade_in_and_out)
            {
                instance.timer = 0f;
                state = FadeState.fading_out;
            }
            else
            {
                EndFade();
            }
        }
    }


    private void HandleFadingOut()
    {
        ComputeAndSetAlpha(1f, 0f);
        if(IsDoneFading())
        {
            EndFade();
        }
    }


    private void EndFade()
    {
        state = FadeState.none;
        if(callback != null)
        {
            callback();
        }
    }


    private bool IsDoneFading()
    {
        return timer >= FADE_TIMING;
    }


    private void ComputeAndSetAlpha(float min, float max)
    {
        float pct = timer / FADE_TIMING;
        float alpha_val = Mathf.Lerp(min, max, pct);
        SetAlpha(alpha_val);
    }


    private void SetAlpha(float alpha_val)
    {
        img.color = new Color(img.color.r, img.color.g, img.color.b, alpha_val);
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second BlackOverlay created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
