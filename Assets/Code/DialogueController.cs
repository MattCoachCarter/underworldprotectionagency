using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueController : MonoBehaviour
{
    private static readonly float SHOW_HIDE_TIME = 0.2f;
    private static readonly float WRITE_TIME = 0.025f;

    public enum DCState {none, showing_but_idle, showing_background, hiding_background, writing, waiting_for_next_chunk, waiting_for_next_text_set};

    [SerializeField] private Image background = null;
    [SerializeField] private List<Text> texts = null;
    [SerializeField] private GameObject more_lines_indicator;
    [SerializeField] private GameObject next_chunk_indicator;


    private DialogueData data;
    private int current_chunk_index;
    private int current_line_index;
    private int current_character_index;
    private int current_text_index;
    private Action finished_callback;
    private DCState state = DCState.none;
    private float timer = 0f;
    private bool close_on_finish = false;



    public void Skip()
    {
        if(state != DCState.none)
        {
            ClearText();
            state = DCState.hiding_background;
        }
    }

    
    public void StartDialogue(DialogueData new_data, Action callback)
    {
        StartDialogue(new_data, callback, true);
    }


    public void StartDialogue(DialogueData new_data, Action callback, bool should_close_on_finish)
    {
        ClearText();
        close_on_finish = should_close_on_finish;
        data = new_data;
        current_chunk_index = -1;
        finished_callback = callback;
        timer = 0f;
        gameObject.SetActive(true);
        more_lines_indicator.SetActive(false);
        next_chunk_indicator.SetActive(false);

        if(state == DCState.showing_but_idle)
        {
            StartText();
        }
        else
        {
            state = DCState.showing_background;
        }
    }


    // Update is called once per frame
    private void Update()
    {
        switch(state)
        {
            case DCState.showing_background:
                DoShowBackground();
                break;
            case DCState.hiding_background:
                DoHideBackground();
                break;
            case DCState.writing:
                DoWrite();
                break;
            case DCState.waiting_for_next_chunk:
                WaitForNextChunk();
                break;
            case DCState.waiting_for_next_text_set:
                WaitForNextTextSet();
                break;
        }
    }


    private void StartText()
    {
        GoToNextChunk();
        timer = 0f;
        state = DCState.writing;
    }


    private void DoShowBackground()
    {
        timer += Time.deltaTime;
        float progress = Mathf.Min((timer / SHOW_HIDE_TIME), 1f);
        background.fillAmount = progress;

        if(progress >= 1f)
        {
            StartText();
        }
    }


    private void DoHideBackground()
    {
        timer += Time.deltaTime;
        float progress = Mathf.Min((timer / SHOW_HIDE_TIME), 1f);
        background.fillAmount = 1f - progress;

        if(progress >= 1f)
        {
            timer = 0f;
            EndDialogue();
        }
    }


    private void DoWrite()
    {
        timer += Time.deltaTime;

        if(Input.GetMouseButtonDown(0))
        {
            ImmediatelyWriteText();
        }
        else if(timer > WRITE_TIME)
        {
            WriteCharacter();
        }
    }


    private void WriteCharacter()
    {
        List<string> current_chunk = data.chunks[current_chunk_index].dialogue;
        string current_line = current_chunk[current_line_index];

        if(current_character_index >= current_line.Length)
        {
            current_line_index++;
            current_character_index = 0;
            if(current_line_index >= current_chunk.Count)
            {
                StartWaitingForNextChunk();
                return;
            }
            current_line = current_chunk[current_line_index];

            current_text_index++;
            if(current_text_index >= texts.Count)
            {
                StartWaitingForNextTextSet();
                return;
            }
        }

        string current_character = current_line[current_character_index].ToString();
        texts[current_text_index].text += current_character;
        current_character_index++;

        if(current_character == " ")
        {
            timer = WRITE_TIME / 2f;
        }
        else
        {
            timer = 0f;
        }
    }


    private void ImmediatelyWriteText()
    {
        List<string> current_chunk = data.chunks[current_chunk_index].dialogue;
        texts[current_text_index].text = current_chunk[current_line_index];

        current_line_index++;
        current_text_index++;
        while(current_line_index < current_chunk.Count && current_text_index < texts.Count)
        {
            texts[current_text_index].text = current_chunk[current_line_index];
            current_line_index++;
            current_text_index++;
        }

        if(current_line_index >= current_chunk.Count)
        {
            StartWaitingForNextChunk();
        }
        else
        {
            StartWaitingForNextTextSet();
        }
    }


    private void StartWaitingForNextChunk()
    {
        next_chunk_indicator.SetActive(true);
        state = DCState.waiting_for_next_chunk;
    }


    private void StartWaitingForNextTextSet()
    {
        more_lines_indicator.SetActive(true);
        state = DCState.waiting_for_next_text_set;
    }


    private bool GetClick()
    {
        return Input.GetMouseButtonDown(0) && !LevelManager.IsEntireGamePaused();
    }


    private void WaitForNextChunk()
    {
        if(GetClick())
        {
            GoToNextChunk();
        }
    }


    private void WaitForNextTextSet()
    {
        if(GetClick())
        {
            GoToNextTextSet();
        }
    }


    private void GoToNextTextSet()
    {
        more_lines_indicator.SetActive(false);
        next_chunk_indicator.SetActive(false);
        current_text_index = 0;
        current_character_index = 0;
        ClearText();
        state = DCState.writing;
    }


    private void GoToNextChunk()
    {
        more_lines_indicator.SetActive(false);
        next_chunk_indicator.SetActive(false);
        ClearText();
        current_chunk_index++;

        if(current_chunk_index < data.chunks.Count)
        {
            current_line_index = 0;
            current_character_index = 0;
            current_text_index = 0;
            state = DCState.writing;
        }
        else
        {
            if(close_on_finish)
            {
                state = DCState.hiding_background;
            }
            else
            {
                state = DCState.showing_but_idle;
                finished_callback();
            }
        }
    }


    private void ClearText()
    {
        foreach(Text text in texts)
        {
            text.text = "";
        }
    }


    private void EndDialogue()
    {
        ClearText();
        state = DCState.none;
        gameObject.SetActive(false);
        if(finished_callback != null)
        {
            finished_callback();
        }
    }
}
