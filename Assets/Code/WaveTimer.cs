using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WaveTimer : MonoBehaviour
{
    public static WaveTimer instance = null;

    [SerializeField] private Text time_text;

    private float timer;
    private bool currently_timing;


    public static void RestartTimer()
    {
        ResetTimer();
        StartTimer();
    }


    public static void ResetTimer()
    {
        if(instance != null)
        {
            instance.timer = 0f;
            instance.ShowTime();
        }
    }


    public static void StartTimer()
    {
        if(instance != null)
        {
            instance.currently_timing = true;
        }
    }


    public static void StopTimer()
    {
        if(instance != null)
        {
            instance.currently_timing = false;
        }
    }



    // Start is called before the first frame update
    private void Start()
    {
        if(HandleSingleton())
        {
            timer = 0f;
            currently_timing = false;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if(currently_timing)
        {
            timer += Time.deltaTime;
            ShowTime();
        }
    }


    private void ShowTime()
    {
        TimeSpan time = TimeSpan.FromSeconds(timer);
        time_text.text = time.ToString("mm':'ss");
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second WaveTimer created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
