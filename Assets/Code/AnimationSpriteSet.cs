using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSpriteSet
{
    public string name;
    public Sprite[] sprites;
    public float sec_duration;

    public AnimationSpriteSet(string _name, Sprite[] _sprites, float duration)
    {
        name = _name;
        sprites = _sprites;
        sec_duration = duration;
    }

    public AnimationSpriteSet(string _name, string spritesheet, float duration)
    {
        name = _name;
        sprites = Common.LoadSpritesheet(spritesheet);
        sec_duration = duration;
    }


    public float GetSecsPerFrame()
    {
        return (sec_duration / (float)sprites.Length);
    }
}