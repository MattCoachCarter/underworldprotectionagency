using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance = null;
    private static readonly string MAIN_CHARACTER_PREFAB = "Prefabs/MainCharacter";
    private static readonly int STARTING_GOLD = 200;

    [SerializeField] private Text level_text;
    [SerializeField] private Text gold_text;
    [SerializeField] private Text wave_text;
    [SerializeField] private Text time_text;
    [SerializeField] private GameObject ui;
    [SerializeField] private GameObject character_display_ui;
    [SerializeField] private GameObject wave_start_ui;
    [SerializeField] private DialogueController main_dialogue_controller;
    [SerializeField] private UnderworldManager underworld_manager;
    [SerializeField] private GameObject tutorial_skip_button;
    [SerializeField] private GameObject in_game_menu;
    [SerializeField] private GameObject dialogue_skip_button;
    [SerializeField] private GameObject dialogue_skip_button_2;
    [SerializeField] private GameObject bottom_ui_panel;
    [SerializeField] private GameObject game_over_screen;
    [SerializeField] private GameObject win_screen;

    private List<GameGrid> grids = null;
    private int current_grid_index = 0;
    private int player_gold = 0;
    private MainCharacter current_main_character = null;
    private GameData game_data = null;
    private int current_level_index = 0;
    private int current_wave_index = 0;
    private bool player_has_control = false;
    private bool tutorial_skipped = false;
    private int pending_wave_index = 0;
    private bool gameplay_paused = false;
    private bool in_pre_wave_setup = false;
    private bool paused = false;


    public static GameGrid GetCurrentGrid()
    {
        return instance.grids[instance.current_grid_index];
    }


    public static int GetCurrentGridIndex()
    {
        return instance.current_grid_index;
    }


    public static bool IsEntireGamePaused()
    {
        return instance.paused;
    }


    public static void Pause()
    {
        instance.paused = true;
        Time.timeScale = 0f;
        instance.in_game_menu.SetActive(true);
    }


    public static void Resume()
    {
        instance.paused = false;
        Time.timeScale = 1f;
        instance.in_game_menu.SetActive(false);
    }


    public static bool InPreWaveSetup()
    {
        return instance.in_pre_wave_setup;
    }


    public static bool IsGameplayPaused()
    {
        return instance.gameplay_paused;
    }


    public static bool PlayerHasControl()
    {
        return instance.player_has_control;
    }


    public static bool IsOnFinalGrid()
    {
        return instance.current_grid_index == 0;
    }


    public static MainCharacter GetCurrentMainCharacter()
    {
        return instance.current_main_character;
    }


    public static LevelData GetCurrentLevelData()
    {
        return instance.game_data.levels[instance.current_level_index];
    }


    public static WaveData GetCurrentWaveData()
    {
        return GetCurrentLevelData().waves[instance.current_wave_index];
    }


    public static void CharacterDefeated()
    {
        WaveTimer.StopTimer();
        instance.PauseGameplay();
    }


    public static void DefeatedCharacterBackAtEntrance()
    {
        instance.PauseGameplay();
        instance.player_has_control = false;
        instance.main_dialogue_controller.StartDialogue(GetCurrentWaveData().post_wave_dialogue,
                                                        MoveCharacterBackThroughEntrance);
        instance.dialogue_skip_button.SetActive(true);
        instance.character_display_ui.SetActive(false);
    }


    public static void MoveCharacterBackThroughEntrance()
    {
        instance.dialogue_skip_button.SetActive(false);
        instance.dialogue_skip_button_2.SetActive(false);
        instance.bottom_ui_panel.SetActive(true);
        GetCurrentMainCharacter().MoveBackUpThroughExit();
        instance.GoToNextWave();
    }


    public static void CharacterReachedStartingPosition()
    {
        instance.StartPreWaveDialogue();
    }


    public static void AddGold(int amount)
    {
        instance.player_gold += amount;
        instance.ShowGoldText();
        if(amount > 0)
        {
            GameObject gold_text_prefab = Common.LoadResource("Prefabs/GoldText", true);
            GameObject gold_text_game_obj = Instantiate(gold_text_prefab,
                                                        GameObject.Find("Canvas").transform,
                                                        false);
            gold_text_game_obj.GetComponent<GoldText>().Setup(amount);
        }
    }


    public static void DeductGold(int amount)
    {
        instance.player_gold -= amount;
        instance.ShowGoldText();
    }


    public static int GetGold()
    {
        return instance.player_gold;
    }


    public static bool CanNavDown()
    {
        return !CameraManager.IsPanning() && 
               GetCurrentGridIndex() > 0 &&
               InPreWaveSetup();
    }


    public static bool CanNavUp()
    {
        return !CameraManager.IsPanning() && 
               GetCurrentGridIndex() < GetCurrentLevelData().starting_grid &&
               InPreWaveSetup();
    }


    public void NavUp()
    {
        if(InPreWaveSetup())
        {
            player_has_control = false;
            DisableUI();
            current_grid_index++;
            MoveCameraToGrid(current_grid_index, NavFinished);
        }
    }


    public void NavDown()
    {
        if(InPreWaveSetup())
        {
            player_has_control = false;
            DisableUI();
            current_grid_index--;
            MoveCameraToGrid(current_grid_index, NavFinished);
        }
    }


    private void NavFinished()
    {
        player_has_control = true;
        EnableUI();
    }


    // Start is called before the first frame update
    private void Start()
    {
        if(HandleSingleton())
        {
            game_data = GameData.Get();
            InitializeScoreAndLevel();
            InitializeGrids();
            BlackOverlay.FadeOut(StartZerothLevel);
        }
    }


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(paused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.RightShift) && Input.GetKeyDown(KeyCode.O))
        {
            AddGold(200);
        }
    }


    public void SkipDialogue()
    {
        main_dialogue_controller.Skip();
    }


    private void StartZerothLevel()
    {
        StartLevel(0);
    }


    private void InitializeGrids()
    {
        grids = new List<GameGrid>();

        List<Tuple<int, int>> blocked_coords_0 = new List<Tuple<int, int>>();
        GameGrid grid_0 = new GameGrid(0f, 1, 6, blocked_coords_0);

        List<Tuple<int, int>> blocked_coords_1 = new List<Tuple<int, int>>();
        blocked_coords_1.Add(new Tuple<int, int>(1, 1));
        blocked_coords_1.Add(new Tuple<int, int>(1, 2));
        blocked_coords_1.Add(new Tuple<int, int>(1, 5));
        blocked_coords_1.Add(new Tuple<int, int>(1, 6));
        blocked_coords_1.Add(new Tuple<int, int>(6, 1));
        blocked_coords_1.Add(new Tuple<int, int>(6, 2));
        blocked_coords_1.Add(new Tuple<int, int>(6, 5));
        blocked_coords_1.Add(new Tuple<int, int>(6, 6));
        blocked_coords_1.Add(new Tuple<int, int>(3, 3));
        blocked_coords_1.Add(new Tuple<int, int>(3, 4));
        blocked_coords_1.Add(new Tuple<int, int>(4, 3));
        blocked_coords_1.Add(new Tuple<int, int>(4, 4));
        GameGrid grid_1 = new GameGrid(192f, 1, 1, blocked_coords_1);

        List<Tuple<int, int>> blocked_coords_2 = new List<Tuple<int, int>>();
        blocked_coords_2.Add(new Tuple<int, int>(0, 3));
        blocked_coords_2.Add(new Tuple<int, int>(1, 3));
        blocked_coords_2.Add(new Tuple<int, int>(2, 3));
        blocked_coords_2.Add(new Tuple<int, int>(3, 3));
        blocked_coords_2.Add(new Tuple<int, int>(5, 3));
        blocked_coords_2.Add(new Tuple<int, int>(6, 3));
        blocked_coords_2.Add(new Tuple<int, int>(7, 3));
        blocked_coords_2.Add(new Tuple<int, int>(0, 4));
        blocked_coords_2.Add(new Tuple<int, int>(1, 4));
        blocked_coords_2.Add(new Tuple<int, int>(2, 4));
        blocked_coords_2.Add(new Tuple<int, int>(3, 4));
        blocked_coords_2.Add(new Tuple<int, int>(5, 4));
        blocked_coords_2.Add(new Tuple<int, int>(6, 4));
        blocked_coords_2.Add(new Tuple<int, int>(7, 4));
        GameGrid grid_2 = new GameGrid(384f, 6, 1, blocked_coords_2);

        grids.Add(grid_0);
        grids.Add(grid_1);
        grids.Add(grid_2);
        current_grid_index = 0;
    }


    private void InitializeScoreAndLevel()
    {
        current_level_index = 0;
        player_gold = STARTING_GOLD;
        ShowLevelText();
        ShowGoldText();
    }


    private void StartLevel(int level_index)
    {
        player_has_control = false;
        DisableUI();
        current_level_index = level_index;
        current_wave_index = 0;
        Store.UnlockObjects(GetCurrentLevelData().unlocks);
        EnterManagerPreLevel();
    }


    private void PositionManagerForTutorialSkip()
    {
        underworld_manager.MoveToTarget(new Vector3(176f, GetManagerY(), 0f), PlaySkipTutorialDialogue);
    }


    private void EnterManagerPreLevel()
    {
        underworld_manager.transform.position = new Vector3(225f, GetManagerY(), 0f);
        underworld_manager.MoveToTarget(new Vector3(176f, GetManagerY(), 0f), ManagerHasEnteredPreLevel);
    }


    private void ManagerHasEnteredPreLevel()
    {
        LevelData level_data = GetCurrentLevelData();
        if(level_data.is_tutorial)
        {
            ShowSkipTutorialButton();
            main_dialogue_controller.StartDialogue(level_data.pre_level_dialogue,
                                                   StartTutorial);
        }
        else if(current_level_index == 1 && tutorial_skipped)
        {
            main_dialogue_controller.StartDialogue(game_data.tutorial_skip_level_1_dialogue,
                                                   ExitManagerPreLevel);
            dialogue_skip_button_2.SetActive(true);
            character_display_ui.SetActive(false);
            bottom_ui_panel.SetActive(false);
        }
        else
        {
            main_dialogue_controller.StartDialogue(level_data.pre_level_dialogue,
                                                   ExitManagerPreLevel);
            dialogue_skip_button_2.SetActive(true);
            character_display_ui.SetActive(false);
            bottom_ui_panel.SetActive(false);
        }
    }


    private void ExitManagerPreLevel()
    {
        dialogue_skip_button.SetActive(false);
        dialogue_skip_button_2.SetActive(false);
        bottom_ui_panel.SetActive(true);
        in_pre_wave_setup = false;
        HideSkipTutorialButton();

        if(GetCurrentLevelData().starting_grid != current_grid_index)
        {
            underworld_manager.MoveToTarget(new Vector3(225f, GetManagerY(), 0f), null);
            MoveCameraToStartingGrid(ManagerHasExitedPreLevel);
        }
        else
        {
            underworld_manager.MoveToTarget(new Vector3(225f, GetManagerY(), 0f), ManagerHasExitedPreLevel);
        }
        current_grid_index = GetCurrentLevelData().starting_grid;
    }


    public static void ManagerHasExitedPreLevel()
    {
        instance.SetupWave(0);
    }


    private void EnterManagerPostLevel()
    {
        in_pre_wave_setup = false;
        underworld_manager.transform.position = new Vector3(225f, GetManagerY(), 0f);
        character_display_ui.SetActive(false);
        underworld_manager.MoveToTarget(new Vector3(176f, GetManagerY(), 0f), ManagerHasEnteredPostLevel);
    }


    private void ManagerHasEnteredPostLevel()
    {
        dialogue_skip_button_2.SetActive(true);
        character_display_ui.SetActive(false);
        bottom_ui_panel.SetActive(false);
        main_dialogue_controller.StartDialogue(GetCurrentLevelData().post_level_dialogue,
                                               ExitManagerPostLevel);
    }


    private void ExitManagerPostLevel()
    {
        dialogue_skip_button.SetActive(false);
        dialogue_skip_button_2.SetActive(false);
        bottom_ui_panel.SetActive(true);
        in_pre_wave_setup = false;
        DisableUI();
        underworld_manager.MoveToTarget(new Vector3(225f, GetManagerY(), 0f), LevelTransition);
        HideSkipTutorialButton();
    }


    public void LevelTransition()
    {
        in_pre_wave_setup = false;
        int new_level_index = current_level_index + 1;
        if(new_level_index >= game_data.levels.Count)
        {
            BlackOverlay.FadeIn(ShowWinScreenAndFadeOut);
        }
        else
        {
            BlackOverlay.FadeInAndOut(GoToNextLevel);
        }
    }


    private void GoToNextWave()
    {
        in_pre_wave_setup = false;
        int new_wave_index = (current_wave_index + 1);
        player_has_control = false;
        PauseGameplay();
        if(GetCurrentLevelData().waves.Count <= new_wave_index)
        {
            EnterManagerPostLevel();
        }
        else
        {
            LevelData level_data = GetCurrentLevelData();
            if(level_data.starting_grid != current_grid_index)
            {
                current_grid_index = level_data.starting_grid;
                pending_wave_index = new_wave_index;
                DisableUI();
                MoveCameraToStartingGrid(SetupPendingWave);
            }
            else
            {
                SetupWave(new_wave_index);
            }
        }
    }


    private void GoToNextLevel()
    {
        in_pre_wave_setup = false;
        int new_level_index = current_level_index + 1;
        StartLevel(new_level_index);
    }


    private void SetupPendingWave()
    {
        EnableUI();
        SetupWave(pending_wave_index);
    }


    private void SetupWave(int wave_index)
    {
        in_pre_wave_setup = false;
        PauseGameplay();
        
        current_wave_index = wave_index;

        AddGold(GetCurrentWaveData().stipend);
        ShowWaveText();
        EnterPreWavePlayerSetup();
    }


    private void EnterPreWavePlayerSetup()
    {
        in_pre_wave_setup = true;
        EnableUI();
        character_display_ui.SetActive(false);
        wave_start_ui.SetActive(true);
        PauseGameplay();
        player_has_control = true;
        WaveTimer.ResetTimer();
    }


    public void EndPreWaveSetup()
    {
        in_pre_wave_setup = false;
        player_has_control = false;
        wave_start_ui.SetActive(false);
        if(current_grid_index != GetCurrentLevelData().starting_grid)
        {
            current_grid_index = GetCurrentLevelData().starting_grid;
            MoveCameraToStartingGrid(SpawnMainCharacter);
        }
        else
        {
            SpawnMainCharacter();
        }
    }


    private void StartPreWaveDialogue()
    {
        main_dialogue_controller.StartDialogue(GetCurrentWaveData().pre_wave_dialogue,
                                               StartWaveGameplay);
        dialogue_skip_button.SetActive(true);
        character_display_ui.SetActive(false);
    }


    private void StartWaveGameplay()
    {
        EnableUI();
        WaveTimer.RestartTimer();
        character_display_ui.SetActive(true);
        UnpauseGameplay();
        GetCurrentMainCharacter().WaveStart();
        player_has_control = true;
        dialogue_skip_button.SetActive(false);
    }


    private void SpawnMainCharacter()
    {
        MainCharacterData main_character_data = GetCurrentWaveData().character_data;
        Vector3 main_character_spawn_position = GetMainCharacterSpawnPosition();
        GameObject main_character_game_object = Common.Spawn(MAIN_CHARACTER_PREFAB, main_character_spawn_position);
        MainCharacter main_character = main_character_game_object.GetComponent<MainCharacter>();
        main_character.Setup(main_character_data);
        current_main_character = main_character;
    }


    private Vector3 GetMainCharacterSpawnPosition()
    {
        return GetCurrentGrid().GetMainCharacterSpawnPosition();
    }


    public static void WaveLost()
    {
        instance.PauseGameplay();
        instance.player_has_control = false;
        WaveTimer.StopTimer();

        if(GetCurrentLevelData().is_tutorial)
        {
            instance.Invoke("RedoTutorialWave", 1.5f);
        }
        else if(IsOnFinalGrid())
        {
            instance.GameLost();
        }
        else
        {
            instance.Invoke("MoveDownAGrid", 0.26f);
        }
    }


    private void MoveDownAGrid()
    {
        current_grid_index--;
        MoveCameraToGrid(current_grid_index, PostMoveDownGrid);
        GetCurrentMainCharacter().ResetToGridEntrance();
        GetCurrentMainCharacter().FadeIn();
    }


    private void PostMoveDownGrid()
    {
        UnpauseGameplay();
        player_has_control = true;
        GetCurrentMainCharacter().WaveStart();
    }


    private void GameLost()
    {
        BlackOverlay.FadeIn(ShowGameOverScreenAndFadeOut);
    }


    private void ShowGameOverScreenAndFadeOut()
    {
        game_over_screen.SetActive(true);
        BlackOverlay.FadeOut(ZeroTimeScale);
    }


    private void ShowWinScreenAndFadeOut()
    {
        win_screen.SetActive(true);
        BlackOverlay.FadeOut(ZeroTimeScale);
    }


    public void RedoTutorialWave()
    {
        TutorialController.RedoTutorialWave();
    }


    public static void ResetForTutorialWaveRetry()
    {
        GetCurrentGrid().RemoveAllObjects();
        instance.player_gold = STARTING_GOLD;
        instance.ShowGoldText();
        instance.EnterPreWavePlayerSetup();
    }


    private void StartTutorial()
    {
        TutorialController.StartTutorial();
    }


    private void ShowLevelText()
    {
        level_text.text = current_level_index.ToString("D2");
    }


    private void ShowWaveText()
    {
        wave_text.text = (current_wave_index + 1).ToString("D2");
    }


    private void ShowGoldText()
    {
        gold_text.text = player_gold.ToString("D2");
    }


    private void PauseGameplay()
    {
        gameplay_paused = true;
        foreach(GameGrid grid in grids)
        {
            grid.DisarmAll();
        }
    }


    private void UnpauseGameplay()
    {
        gameplay_paused = false;
        foreach(GameGrid grid in grids)
        {
            grid.ArmAll();
        }
    }


    private void DisableUI()
    {
        ui.SetActive(false);
    }


    private void EnableUI()
    {
        ui.SetActive(true);
    }


    private void ShowSkipTutorialButton()
    {
        if(tutorial_skip_button != null)
        {
            tutorial_skip_button.SetActive(true);
        }
    }


    private void HideSkipTutorialButton()
    {
        if(tutorial_skip_button != null)
        {
            tutorial_skip_button.SetActive(false);
        }
    }


    public void SkipTutorial()
    {
        tutorial_skipped = true;
        TutorialController.Skip();
        PositionManagerForTutorialSkip();
    }


    private void PlaySkipTutorialDialogue()
    {
        main_dialogue_controller.StartDialogue(game_data.tutorial_skip_dialogue,
                                               ExitManagerPostLevel);
        dialogue_skip_button.SetActive(true);
    }


    private void StartPostTutorialLevel()
    {
        StartLevel(1);
    }


    private void MoveCameraToStartingGrid(Action callback)
    {
        MoveCameraToGrid(GetCurrentLevelData().starting_grid, callback);
    }


    private void MoveCameraToGrid(int grid_index, Action callback)
    {
        CameraManager.PanToGrid(grid_index, callback);
    }


    private float GetManagerY()
    {
        if(current_grid_index == 1)
        {
            return 229f;
        }
        else if(current_grid_index == 2)
        {
            return 421f;
        }
        return 37f;
    }


    public void ReturnToMainMenu()
    {
        GameManager.LoadMainScene();
        Time.timeScale = 1f;
    }


    public void ZeroTimeScale()
    {
        Time.timeScale = 0f;
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be
     * only one
     */
    private bool HandleSingleton()
    {
        if(instance != null)
        {
            // Don't spawn, there's already a singleton!
            Debug.LogWarning("Second LevelManager created...");
            Destroy(gameObject);
            return false;
        }

        instance = this;
        return true;
    }


    private void OnDestroy()
    {
        instance = null;
    }
}
