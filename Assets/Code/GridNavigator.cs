using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GridNavigator : MonoBehaviour
{
    [SerializeField] private GameObject button_game_object;
    [SerializeField] private bool is_down;


    private void Update()
    {
        CheckIfShouldShowButton();
    }


    private void CheckIfShouldShowButton()
    {
        if(is_down)
        {
            button_game_object.SetActive(CanNavDown());
        }
        else
        {
            button_game_object.SetActive(CanNavUp());
        }
    }


    private bool CanNavDown()
    {
        return LevelManager.CanNavDown();
    }


    private bool CanNavUp()
    {
        return LevelManager.CanNavUp();
    }
}
