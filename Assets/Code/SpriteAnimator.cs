using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SpriteAnimator : MonoBehaviour
{
    [SerializeField] private bool image_mode = false;

    private bool is_setup = false;
    private Dictionary<string, AnimationSpriteSet> animation_sets = new Dictionary<string, AnimationSpriteSet>();
    private SpriteRenderer sprite_renderer = null;
    private Image image = null;
    private string default_animation = null;
    private AnimationSpriteSet current_animation = null;
    private string current_animation_name = null;
    private int current_sprite_id = 0;
    private float sec_per_frame;
    private bool running = false;
    private IEnumerator current_coroutine = null;
    private Action callback;
    private bool pause_after_animation = false;
    private bool coroutine_delayed = false;
    private float animation_timing_modifier = 1f;


    public void Start()
    {
        DoStart();
    }


    public void Update()
    {
        if(coroutine_delayed && IsObjActive())
        {
            StartCoroutine(current_coroutine);
            coroutine_delayed = false;
        }
    }


    private void DoStart()
    {
        GetRenderable();
        if(IsReady())
        {
            running = true;
            PlayDefault();
        }
    }


    public void SetAnimationTimingModifier(float new_val)
    {
        animation_timing_modifier = new_val;
    }


    public float GetAnimationTimingModifier()
    {
        return animation_timing_modifier;
    }


    public void Setup(List<AnimationSpriteSet> animations, string default_animation_name)
    {
        StopCurrentCoroutine();
        animation_sets = new Dictionary<string, AnimationSpriteSet>();

        foreach(AnimationSpriteSet animation in animations)
        {
            AddAnimationSet(animation);
        }
        SetDefaultAnimation(default_animation_name);

        if(!running)
        {
            DoStart();
        }
        else
        {
            PlayDefault();
        }

        is_setup = true;
    }


    public bool IsSetup()
    {
        return is_setup;
    }


    public string GetCurrentAnimationName()
    {
        return current_animation_name;
    }


    private void GetRenderable()
    {
        if(image_mode)
        {
            image = GetComponent<Image>();
        }
        else
        {
            sprite_renderer = GetComponent<SpriteRenderer>();
        }
    }


    private void SetSprite(Sprite spr)
    {
        if(image == null && sprite_renderer == null)
        {
            GetRenderable();
        }

        if(image_mode && image != null)
        {
            image.sprite = spr;
        }
        else if(sprite_renderer != null)
        {
            sprite_renderer.sprite = spr;
        }
        else
        {
            Debug.LogWarning("Trying to set sprite, but sprite renderer and image are null");
        }
    }


    public void SetDefaultAnimation(string animation_name)
    {
        default_animation = animation_name;
        SetSprite(animation_sets[default_animation].sprites[0]);
    }


    private void AddAnimationSet(AnimationSpriteSet animation_set)
    {
        animation_sets.Add(animation_set.name, animation_set);
    }


    private bool HasRenderable()
    {
        return sprite_renderer != null || image != null;
    }


    public bool IsReady()
    {
        return (default_animation != null && HasRenderable());
    }


    public void Stop()
    {
        running = false;
        StopCurrentCoroutine();
    }


    public void PlayAnimation(string animation_name)
    {
        PlayAnimation(animation_name, null);
    }


    public void PlayAnimationAndPause(string animation_name)
    {
        PlayAnimationAndPause(animation_name, null);
    }


    public void PlayAnimationAndPause(string animation_name, Action _callback)
    {
        pause_after_animation = true;
        PlayAnimation(animation_name, _callback);
    }


    public void PlayAnimation(string animation_name, Action _callback)
    {
        if(running)
        {
            // Make sure we know about what animation was requested of us
            if(animation_sets.ContainsKey(animation_name))
            {
                SetCallback(_callback);
                current_animation = animation_sets[animation_name];
                current_animation_name = animation_name;
                current_sprite_id = 0;
                sec_per_frame = current_animation.GetSecsPerFrame();
                SafeStartCoroutine();
            }
            else
            {
                Debug.LogError("Tried to play animation: "+ animation_name +" but that is not a valid animation for this object.");
                Debug.Log("Playing default animation instead: "+ default_animation);
                PlayDefault();
            }
        }
    }


    public void SetCallback(Action _callback)
    {
        // Before stomping over the current callback, we should see if we already have
        // a callback, and call it if we haven't already
        if(callback != null)
        {
            callback();
        }
        callback = _callback;
    }


    public void PlayDefault()
    {
        PlayAnimation(default_animation);
    }


    private bool IsObjActive()
    {
        if(image_mode)
        {
            return gameObject.activeInHierarchy;
        }
        return gameObject.activeSelf;
    }


    private void SafeStartCoroutine()
    {
        StopCurrentCoroutine();
        current_coroutine = AnimateSprite();
        if(IsObjActive())
        {
            StartCoroutine(current_coroutine);
        }
        else
        {
            coroutine_delayed = true;
        }
    }


    private void StopCurrentCoroutine()
    {
        if(current_coroutine != null)
        {
            StopCoroutine(current_coroutine);
            current_coroutine = null;
        }
    }


    IEnumerator AnimateSprite()
    {
        if(running)
        {
            yield return new WaitForSeconds(sec_per_frame * animation_timing_modifier);
            SetSprite(current_animation.sprites[current_sprite_id]);
            current_sprite_id++;
            if(current_sprite_id >= current_animation.sprites.Length)
            {
                if(callback != null)
                {
                    callback();
                    callback = null;
                }
                if(!pause_after_animation)
                {
                    yield return new WaitForSeconds(sec_per_frame * animation_timing_modifier);
                    PlayDefault();
                }
                else
                {
                    pause_after_animation = false;
                }
            }
            else 
            {
                yield return new WaitForSeconds(sec_per_frame * animation_timing_modifier);
                SafeStartCoroutine();
            }
        }
    }
}
