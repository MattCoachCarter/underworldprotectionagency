using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverableGameObject : MonoBehaviour
{
    protected bool hovering = false;
    protected bool last_frame_hover_status = false;
    protected List<HoverableGameObject> hover_children = null;


    public void AddHoverChild(HoverableGameObject c)
    {
        hover_children.Add(c);
    }


    public void RemoveHoverChild(HoverableGameObject c)
    {
        hover_children.Remove(c);
    }


    protected bool AnyHovering()
    {
        if(hovering)
        {
            return true;
        }
        else
        {
            foreach(HoverableGameObject child in hover_children)
            {
                if(child.AnyHovering())
                {
                    return true;
                }
            }
        }

        return false;
    }


    protected virtual void Start()
    {
        hovering = false;
        last_frame_hover_status = false;
        hover_children = new List<HoverableGameObject>();
    }


    protected virtual void Update()
    {
        bool this_frame_hover_status = AnyHovering();
        if(last_frame_hover_status && !this_frame_hover_status)
        {
            OnUnhover();
        }
        else if(!last_frame_hover_status && this_frame_hover_status)
        {
            OnHover();
        }

        last_frame_hover_status = this_frame_hover_status;
    }


    protected virtual void OnHover()
    {
        // OVERRIDE ME!
    }


    protected virtual void OnUnhover()
    {
        // OVERRIDE ME!
    }


    void OnMouseEnter()
    {
        hovering = true;
    }

    
    void OnMouseExit()
    {
        hovering = false;
    }
}
