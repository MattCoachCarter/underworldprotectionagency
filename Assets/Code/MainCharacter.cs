using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Direction {none, up, down, left, right}
public enum Debuff {none, frozen, amplify}

public class MainCharacter : MonoBehaviour
{
    public enum MCState {none, idle, free_moving, moving, attacking, dead};
    public enum MCLogic {none, move_to_exit, wait_for_wave_start, return_to_entrance, exit};

    private MainCharacterData data;
    private SpriteRenderer sprite_renderer;
    private SpriteAnimator sprite_animator;
    private Rigidbody2D rbody;
    private float fade_timer = 0f;
    private bool fading_in = false;
    private bool fading_out = false;
    private bool damageable = true;
    private Direction facing = Direction.none;
    private Space space;
    private Space target_space;
    private GameGrid grid;
    private MCState state = MCState.none;
    private MCLogic pending_logic = MCLogic.none;
    private Dictionary<Debuff, float> debuffs;
    private float move_penalty = 1f;
    private float move_bonus = 1f;


    public void Setup(MainCharacterData d)
    {
        data = d;
        data.stats.health = data.stats.max_health;
        move_penalty = 1f;
        move_bonus = 1f;
        sprite_renderer = GetComponentInChildren<SpriteRenderer>();
        sprite_animator = GetComponentInChildren<SpriteAnimator>();
        rbody= GetComponent<Rigidbody2D>();
        debuffs = new Dictionary<Debuff, float>();
        SetUpAnimations();
        grid = LevelManager.GetCurrentGrid();
    }


    public Space GetSpace()
    {
        return space;
    }


    public void ResetToGridEntrance()
    {
        grid = LevelManager.GetCurrentGrid();
        Space entrance_space = grid.GetEntrance();
        transform.position = entrance_space.transform.position;
        space = entrance_space;
    }


    public void AddDebuff(Debuff debuff_name)
    {
        switch(debuff_name)
        {
            case Debuff.frozen:
                DoFrozenDebuff();
                break;
            case Debuff.amplify:
                DoAmplifyDebuff();
                break;
            default:
                break;
        }
    }


    public void RemoveDebuff(Debuff debuff_name)
    {
        switch(debuff_name)
        {
            case Debuff.frozen:
                DoFrozenUndebuff();
                break;
            case Debuff.amplify:
                DoAmplifyUndebuff();
                break;
            default:
                break;
        }
    }


    public MainCharacterData GetData()
    {
        return data;
    }


    public int GetX()
    {
        return space.GetX();
    }


    public int GetY()
    {
        return space.GetY();
    }


    public Space GetTargetSpace()
    {
        return target_space;
    }


    public int GetTargetX()
    {
        return target_space.GetX();
    }


    public int GetTargetY()
    {
        return target_space.GetY();
    }


    public float GetMoveSpeed()
    {
        return data.stats.speed * move_penalty * move_bonus;
    }


    public void WaveStart()
    {
        pending_logic = MCLogic.move_to_exit;
        MoveCloserToExit();
    }


    public void MoveBackUpThroughExit()
    {
        move_bonus = 10f;
        SetStateAndFacing(MCState.free_moving, Direction.up);
        FadeOut();
        Invoke("DestroySelf", 2f);
    }


    // Start is called before the first frame update
    private void Start()
    {
        facing = Direction.down;
        FadeIn();
        MoveToStart();
    }


    // Update is called once per frame
    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.RightShift) && Input.GetKeyDown(KeyCode.P))
        {
            TakeBaseDamage(200);
        }

        HandleFading();
        ManageDebuffs();
    }


    // Update is called once per frame
    private void FixedUpdate()
    {
        HandleMovement();
    }


    private void HandleMovement()
    {
        if(state == MCState.moving || state == MCState.free_moving)
        {
            float move_x = 0f;
            float move_y = 0f;

            if(facing == Direction.down)
            {
                move_y = -1f;
            }
            else if(facing == Direction.up)
            {
                move_y = 1f;
            }
            if(facing == Direction.left)
            {
                move_x = -1f;
            }
            else if(facing == Direction.right)
            {
                move_x = 1f;
            }

            float move_magnitude = GetMoveSpeed() * Time.fixedDeltaTime;
            if(pending_logic == MCLogic.wait_for_wave_start)
            {
                move_magnitude = 30f * Time.fixedDeltaTime;
            }

            if(state == MCState.moving)
            {
                float distance_to_target = GetDistanceToTargetSpace();
                if(distance_to_target < move_magnitude)
                {
                    move_magnitude = distance_to_target;
                }
            }

            Vector3 move_vector = new Vector3(move_x, move_y, 0) * move_magnitude;
            Vector3 new_position = transform.position + move_vector;
            rbody.MovePosition(new_position);

            if(state == MCState.moving && GetDistanceToTargetSpace() < 0.5f)
            {
                ReachedMoveTarget();
            }
        }
    }


    private float GetDistanceToTargetSpace()
    {
        Vector3 my_pos = new Vector3(transform.position.x, transform.position.y, 0f);
        Vector3 target_pos = new Vector3(target_space.transform.position.x, target_space.transform.position.y, 0f);
        return Vector3.Distance(my_pos, target_pos);
    }


    private void SnapToClosestSpace()
    {
        float distance_to_space = Vector3.Distance(transform.position, space.transform.position);
        float distance_to_target_space = Vector3.Distance(transform.position, target_space.transform.position);
        if(distance_to_space > distance_to_target_space)
        {
            space = target_space;
            transform.position = target_space.transform.position;
        }
        else
        {
            transform.position = space.transform.position;
        }
    }


    private void ReachedMoveTarget()
    {
        space = target_space;
        transform.position = space.transform.position;
        if(space == grid.GetExit())
        {
            ReachedExit();
        }

        switch(pending_logic)
        {
            case MCLogic.wait_for_wave_start:
                NotifyLevelManagerWeAreReady();
                break;
            case MCLogic.move_to_exit:
                MoveCloserToExit();
                break;
            case MCLogic.exit:
                if(LevelManager.IsOnFinalGrid())
                {
                    SetStateAndFacing(MCState.free_moving, Direction.down);
                    Invoke("FadeOut", 0.25f);
                }
                else
                {
                    SetStateAndFacing(MCState.idle, Direction.down);
                    FadeOut();
                }
                
                LevelManager.WaveLost();
                break;
            case MCLogic.return_to_entrance:
                if(space == grid.GetEntrance())
                {
                    LevelManager.DefeatedCharacterBackAtEntrance();
                    SetStateAndFacing(MCState.idle, Direction.down);
                }
                else
                {
                    MoveBackToEntrance();
                }
                break;
            case MCLogic.none:
                Idle();
                break;
        }
    }


    private void Idle()
    {
        SetStateAndFacing(MCState.idle, Direction.down);
    }


    private void MoveToStart()
    {
        pending_logic = MCLogic.wait_for_wave_start;
        target_space = grid.GetEntrance();
        SetStateAndFacing(MCState.moving, Direction.down);
    }


    private void ReachedExit()
    {
        pending_logic = MCLogic.exit;
    }


    private void NotifyLevelManagerWeAreReady()
    {
        LevelManager.CharacterReachedStartingPosition();
        SetStateAndFacing(MCState.idle, Direction.down);
    }


    private void SetStateAndFacing(MCState new_state, Direction new_facing)
    {
        state = new_state;
        facing = new_facing;
        SetAnimationByStateAndFacing();
    }


    private void SetAnimationByStateAndFacing()
    {
        string animation_direction = "down";
        if(facing == Direction.up)
        {
            animation_direction = "up";
        }
        else if(facing != Direction.down)
        {
            animation_direction = "side";
        }

        string animation_name = "idle";
        switch(state)
        {
            case MCState.moving:
                animation_name = "walk";
                break;
            case MCState.free_moving:
                animation_name = "walk";
                break;
            case MCState.attacking:
                animation_name = "attack";
                break;
            case MCState.dead:
                animation_name = "dead";
                break;
            default:
                animation_name = "idle";
                break;
        }

        bool sprite_flip = false;
        if(facing == Direction.left)
        {
            sprite_flip = true;
        }


        string animation_to_play = animation_name +"_"+ animation_direction;
        sprite_renderer.flipX = sprite_flip;
        LoopAnimation(animation_to_play);
    }


    private void HeadToSpace(Space space_to_head_to)
    {
        Direction move_direction = space.GetDirection(space_to_head_to);
        target_space = space_to_head_to;
        if(state != MCState.moving || facing != move_direction)
        {
            SetStateAndFacing(MCState.moving, move_direction);
        }
    }


    private void MoveCloserToExit()
    {
        MoveToUltimateTarget(grid.GetExit());
    }


    private void MoveBackToEntrance()
    {
        MoveToUltimateTarget(grid.GetEntrance());
    }


    private void MoveToUltimateTarget(Space ultimate_target)
    {
        HeadToSpace(GetNextMoveTarget(ultimate_target));
    }


    private Space GetNextMoveTarget(Space ultimate_target)
    {
        List<Space> path = AStarSearch(space, ultimate_target);
        if(path == null || path.Count <= 0)
        {
            return ultimate_target;
        }
        return path[0];
    }


    private List<Space> ReconstructAStarPath(Space current_node, Dictionary<Space, Space> came_from)
    {
        List<Space> path = new List<Space>();
        while(came_from.ContainsKey(current_node))
        {
            path.Add(current_node);
            current_node = came_from[current_node];
        }

        path.Reverse();

        /*
        string path_str = "";
        foreach(Space s in path)
        {
            if(path_str != "")
            {
                path_str += " > ";
            }
            path_str += "("+ s.GetX() +", "+ s.GetY() +")";
        }
        Debug.Log("<"+ space.GetX() +", "+ space.GetY() +"> : "+ path_str);
        */

        return path;
    }


    private List<Space> AStarSearch(Space source, Space target)
    {
        List<Space> open_list = new List<Space>() {source};
        Dictionary<Space, Space> came_from = new Dictionary<Space, Space>();

        Dictionary<Space, float> g_scores = new Dictionary<Space, float>();
        Dictionary<Space, float> f_scores = new Dictionary<Space, float>();
        for(int y = 0; y < GameConfig.GRID_HEIGHT; y++)
        {
            for(int x = 0; x < GameConfig.GRID_WIDTH; x++)
            {
                Space s = grid.GetSpace(x, y);
                g_scores[s] = 99998f;
                f_scores[s] = 99998f;
            }
        }

        g_scores[source] = 0f;
        f_scores[source] = source.GetDistance(target);


        while(open_list.Count > 0)
        {
            // Determine current by node in the open list with lowest distance
            // to target
            Space current_node = open_list[0];
            float lowest_f_score = 99999f;
            foreach(Space open_node in open_list)
            {
                if(f_scores[open_node] < lowest_f_score)
                {
                    current_node = open_node;
                    lowest_f_score = f_scores[open_node];
                }
            }

            open_list.Remove(current_node);

            // If the current node is the target, we are done
            if(current_node == target)
            {
                return ReconstructAStarPath(current_node, came_from);
            }
            else
            {
                foreach(Space neighbor_node in grid.GetUnblockedNeighbors(current_node))
                {
                    float tentative_g_score = g_scores[current_node] + 1f;
                    if(tentative_g_score < g_scores[neighbor_node])
                    {
                        came_from[neighbor_node] = current_node;
                        g_scores[neighbor_node] = tentative_g_score;
                        f_scores[neighbor_node] = tentative_g_score + neighbor_node.GetDistance(target);
                        if(!open_list.Contains(neighbor_node))
                        {
                            open_list.Add(neighbor_node);
                        }
                    }
                }
            }
        }

        Debug.LogError("A* Failure!");
        return null;
    }


    private void HandleFading()
    {
        fade_timer += Time.deltaTime;
        if(fading_in || fading_out)
        {
            float time_adjustment = fade_timer / GameConfig.MAIN_CHARACTER_FADE_TIMING;

            float start_alpha = 0f;
            float end_alpha = 1f;
            if(fading_out)
            {
                start_alpha = 1f;
                end_alpha = 0f;
            }
            
            float current_alpha = Mathf.Lerp(start_alpha, end_alpha, time_adjustment);
            sprite_renderer.color = new Color(sprite_renderer.color.r,
                                              sprite_renderer.color.g,
                                              sprite_renderer.color.b,
                                              current_alpha);
        }

        if(fade_timer >= GameConfig.MAIN_CHARACTER_FADE_TIMING)
        {
            fading_in = false;
            fading_out = false;
        }
    }


    public void FadeIn()
    {
        fading_in = true;
        fading_out = false;
        fade_timer = 0f;
    }


    public void FadeOut()
    {
        fading_out = true;
        fading_in = false;
        fade_timer = 0f;
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        switch(other.tag)
        {
            case "hurtbox":
                EnterHurtbox(other.gameObject);
                break;
            default:
                break;
        }
    }


    private void EnterHurtbox(GameObject hurtbox_game_object)
    {
        Hurtbox hurtbox = hurtbox_game_object.GetComponent<Hurtbox>();
        if(hurtbox.IsPlayerControlled())
        {
            GetHurtFromHurtbox(hurtbox);
        }
    }


    private float GetBaseDamageMultiplier()
    {
        float multiplier = 1f;
        if(HaveDebuff(Debuff.amplify))
        {
            multiplier = 2f;
        }

        return multiplier;
    }


    private void GetHurtFromHurtbox(Hurtbox hurtbox)
    {
        if(!damageable) { return; }

        int base_damage_done = (int)Mathf.Round(((float)hurtbox.MaybeDamage(gameObject)) * GetBaseDamageMultiplier());
        if(base_damage_done > 0 && !hurtbox.IsTrigger())
        {
            TakeBaseDamage(base_damage_done);
        }
    }


    private void TakeBaseDamage(int base_damage)
    {
        int effective_damage = Mathf.Max((base_damage - data.stats.defense), 0);
        data.stats.health -= effective_damage;

        DoDamageText(effective_damage);

        if(data.stats.health <= 0)
        {
            data.stats.health = 0;
            move_bonus = 10f;
            pending_logic = MCLogic.return_to_entrance;
            SnapToClosestSpace();
            SetStateAndFacing(MCState.dead, Direction.down);
            LevelManager.CharacterDefeated();
            damageable = false;
            Invoke("MoveBackToEntrance", 0.5f);
        }
    }


    private void ManageDebuffs()
    {
        List<Debuff> debuffs_to_remove = new List<Debuff>();
        List<Debuff> all_debuffs = new List<Debuff>(debuffs.Keys);
        foreach(Debuff debuff_name in all_debuffs)
        {
            debuffs[debuff_name] -= Time.deltaTime;
            if(debuffs[debuff_name] <= 0f)
            {
                debuffs_to_remove.Add(debuff_name);
            }
        }

        foreach(Debuff debuff_to_remove in debuffs_to_remove)
        {
            RemoveDebuff(debuff_to_remove);
        }
    }


    private bool HaveDebuff(Debuff debuff_name)
    {
        return debuffs.ContainsKey(debuff_name);
    }


    private void AddDebuffToMap(Debuff debuff_name, float debuff_duration)
    {
        debuffs[debuff_name] = debuff_duration;
    }


    private void RemoveDebuffFromMap(Debuff debuff_name)
    {
        if(debuffs.ContainsKey(debuff_name))
        {
            debuffs.Remove(debuff_name);
        }
    }


    private void AdjustMovePenalty(float amount)
    {
        move_penalty = Mathf.Clamp((move_penalty + amount), 0.1f, 1f);
    }


    private void AdjustAnimationTiming(float amount)
    {
        sprite_animator.SetAnimationTimingModifier(sprite_animator.GetAnimationTimingModifier() + amount);
    }


    private void AdjustColor(float r_adjust, float g_adjust, float b_adjust, float a_adjust)
    {
        sprite_renderer.color = new Color(sprite_renderer.color.r + r_adjust,
                                          sprite_renderer.color.g + g_adjust,
                                          sprite_renderer.color.b + b_adjust,
                                          sprite_renderer.color.a + a_adjust);
    }


    private void DoFrozenDebuff()
    {
        if(!HaveDebuff(Debuff.frozen))
        {
            DoDamageText("frozen", new Color(0.5f, 0.6f, 1f));
            AdjustMovePenalty(-0.5f);
            AdjustColor(-0.5f, -0.4f, 0f, 0f);
            AdjustAnimationTiming(1f);
        }
        AddDebuffToMap(Debuff.frozen, 3f);
    }


    private void DoFrozenUndebuff()
    {
        RemoveDebuffFromMap(Debuff.frozen);
        AdjustMovePenalty(0.5f);
        AdjustColor(0.5f, 0.4f, 0f, 0f);
        AdjustAnimationTiming(-1f);
    }


    private void DoAmplifyDebuff()
    {
        if(!HaveDebuff(Debuff.amplify))
        {
            DoDamageText("damage amplified", new Color(1f, 1f, 0.4f));
            AdjustColor(0f, 0f, -0.6f, 0f);
        }
        AddDebuffToMap(Debuff.amplify, 3f);
    }


    private void DoAmplifyUndebuff()
    {
        RemoveDebuffFromMap(Debuff.amplify);
        AdjustColor(0f, 0f, 0.5f, 0f);
    }


    private void DestroySelf()
    {
        Common.SafeDestroy(gameObject);
    }


    private void SetUpAnimations()
    {
        sprite_animator.Setup(GetAnimations(), "idle_down");
    }


    private void PlayAnimation(string animation_name)
    {
        sprite_animator.PlayAnimation(animation_name);
    }


    private void LoopAnimation(string animation_name)
    {
        sprite_animator.SetDefaultAnimation(animation_name);
        sprite_animator.PlayAnimation(animation_name);
    }


    private List<AnimationSpriteSet> GetAnimations()
    {
        string animation_directory = "Spritesheets/"+ data.animation_key +"/";
        List<AnimationSpriteSet> animations = new List<AnimationSpriteSet>();
        animations.Add(new AnimationSpriteSet("idle_down",
                                              animation_directory +"idle_down",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("idle_up",
                                              animation_directory +"idle_up",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("idle_side",
                                              animation_directory +"idle_side",
                                              0.5f));
        animations.Add(new AnimationSpriteSet("attack_down",
                                              animation_directory +"attack_down",
                                              0.1f));
        animations.Add(new AnimationSpriteSet("attack_up",
                                              animation_directory +"attack_up",
                                              0.1f));
        animations.Add(new AnimationSpriteSet("attack_side",
                                              animation_directory +"attack_side",
                                              0.1f));
        animations.Add(new AnimationSpriteSet("walk_down",
                                              animation_directory +"walk_down",
                                              0.25f));
        animations.Add(new AnimationSpriteSet("walk_up",
                                              animation_directory +"walk_up",
                                              0.25f));
        animations.Add(new AnimationSpriteSet("walk_side",
                                              animation_directory +"walk_side",
                                              0.25f));
        animations.Add(new AnimationSpriteSet("dead_down",
                                              animation_directory +"dead_down",
                                              0.25f));

        return animations;
    }


    public void DoDamageText(int dmg)
    {
        GameObject dmg_text_prefab = Common.LoadResource("Prefabs/DamageText", true);
        GameObject damage_text_game_object = Instantiate(dmg_text_prefab,
                                                         GameObject.Find("Canvas").transform,
                                                         false);

        DamageText damage_text = damage_text_game_object.GetComponent<DamageText>();
        float x_offset = UnityEngine.Random.Range(-6f, -6f);
        float y_offset = UnityEngine.Random.Range(-6f, -6f);
        Vector3 damage_text_pos = new Vector3(transform.position.x + x_offset,
                                              transform.position.y + y_offset,
                                              transform.position.z);
        if(dmg > 0)
        {
            damage_text.Setup(dmg, damage_text_pos);
        }
        else
        {
            damage_text.Setup("blocked", damage_text_pos);
        }
    }


    public void DoDamageText(string txt)
    {
        DoDamageText(txt, new Color(1f, 1f, 1f));
    }


    public void DoDamageText(string txt, Color c)
    {
        GameObject dmg_text_prefab = Common.LoadResource("Prefabs/DamageText", true);
        GameObject damage_text_game_object = Instantiate(dmg_text_prefab,
                                                         GameObject.Find("Canvas").transform,
                                                         false);

        DamageText damage_text = damage_text_game_object.GetComponent<DamageText>();
        float x_offset = UnityEngine.Random.Range(-6f, -6f);
        float y_offset = UnityEngine.Random.Range(-6f, -6f);
        Vector3 damage_text_pos = new Vector3(transform.position.x + x_offset,
                                              transform.position.y + y_offset,
                                              transform.position.z);
        damage_text.Setup(txt, damage_text_pos, c);
    }
}
