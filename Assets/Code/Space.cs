using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Space : HoverableGameObject
{
    [SerializeField] private Sprite normal_sprite;
    [SerializeField] private Sprite hover_sprite;
    [SerializeField] private Sprite not_allowed_sprite;

    private PlayerObject player_object = null;
    private SpriteRenderer sprite_renderer = null;
    private int grid_x = -1;
    private int grid_y = -1;
    private bool unplaceable = false;
    private bool permablocked = false;
    private GameGrid parent;


    public GameGrid GetParentGrid()
    {
        return parent;
    }


    public PlayerObject GetPlayerObject()
    {
        return player_object;
    }


    public void Arm()
    {
        if(player_object != null)
        {
            player_object.Arm();
        }
    }


    public void Disarm()
    {
        if(player_object != null)
        {
            player_object.Disarm();
        }
    }


    public void RemoveObject()
    {
        if(player_object != null)
        {
            Common.SafeDestroy(player_object.gameObject);
            player_object = null;
        }
    }


    public void SetPlayerObject(GameObject player_object_game_object)
    {
        if(player_object != null)
        {
            Debug.LogError("Trying to set player object for space, but this space already has a player object");
        }

        player_object_game_object.transform.position = new Vector3(transform.position.x,
                                                                   transform.position.y,
                                                                   (transform.position.z + 2f));
        player_object = player_object_game_object.GetComponent<PlayerObject>();
        player_object.Setup(this);
        AddHoverChild(player_object_game_object.GetComponent<HoverableGameObject>());
        if(LevelManager.IsGameplayPaused())
        {
            player_object.Disarm();
        }
    }


    public int GetX()
    {
        return grid_x;
    }


    public int GetY()
    {
        return grid_y;
    }


    public bool IsAtCoords(int x, int y)
    {
        return (GetX() == x && GetY() == y);
    }


    public bool IsEmpty()
    {
        return player_object == null;
    }


    public bool IsBlocked()
    {
        return permablocked || (player_object != null && player_object.IsBlocking());
    }


    public bool IsOccupied()
    {
        return player_object != null;
    }


    public bool IsUnplaceable()
    {
        return unplaceable;
    }


    public float GetDistance(Space other)
    {
        return Vector2.Distance(new Vector2(GetX(), GetY()),
                                new Vector2(other.GetX(), other.GetY()));
    }


    public Direction GetDirection(Space other)
    {
        if(other.GetX() > GetX())
        {
            return Direction.right;
        }
        else if(other.GetX() < GetX())
        {
            return Direction.left;
        }
        else if(other.GetY() < GetY())
        {
            return Direction.down;
        }
        else if(other.GetY() > GetY())
        {
            return Direction.up;
        }

        return Direction.none;
    }


    public void Setup(GameGrid p, int _grid_x, int _grid_y, bool _unplaceable, bool _permablocked)
    {
        parent = p;
        grid_x = _grid_x;
        grid_y = _grid_y;
        unplaceable = _unplaceable;
        permablocked = _permablocked;
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        sprite_renderer = GetComponent<SpriteRenderer>();
        if(IsUnplaceable())
        {
            sprite_renderer.enabled = false;
        }
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        CheckForClick();
    }


    private bool GetClick()
    {
        return Input.GetMouseButtonDown(0) && !LevelManager.IsEntireGamePaused();
    }


    private void CheckForClick()
    {
        if(GetClick() && AnyHovering() && LevelManager.PlayerHasControl())
        {
            PlayerControlContext.SelectSpace(this);
            ShowContextualHoverSprite();
        }
    }


    protected override void OnHover()
    {
        if(!IsUnplaceable())
        {
            ShowContextualHoverSprite();
        }
    }


    protected override void OnUnhover()
    {
        if(!IsUnplaceable())
        {
            ShowNormalSprite();
        }
    }


    private void ShowContextualHoverSprite()
    {
        if(PlayerControlContext.PrefabIsSelected() && LevelManager.PlayerHasControl())
        {
            if(PlayerControlContext.IsSpaceAllowed(this))
            {
                ShowHoverSprite();
            }
            else
            {
                ShowNotAllowedSprite();
            }
        }
    }


    private void ShowHoverSprite()
    {
        sprite_renderer.sprite = hover_sprite;
    }


    private void ShowNotAllowedSprite()
    {
        sprite_renderer.sprite = not_allowed_sprite;
    }


    private void ShowNormalSprite()
    {
        sprite_renderer.sprite = normal_sprite;
    }
}
