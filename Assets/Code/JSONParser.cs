﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;


public static class JSONParser
{
    private static readonly string GAME_DATA_FILE_NAME = "game_data.json";
    private static readonly string LEVELS_DIR_NAME = "levels";
    private static readonly string USER_DATA_FILE = "user_data.json";


    public static T ParseJSONFile<T>(string file_path)
    {
        Debug.Log("Loading file as JSON: "+ file_path);
        return ParseJSON<T>(ReadFile(file_path));
    }


    public static GameData ParseGameData()
    {
        return ParseJSONFile<GameData>(AddStreamingAssetsPath(GAME_DATA_FILE_NAME));
    }


    public static List<LevelData> GetLevels()
    {
        List<LevelData> all_level_data = new List<LevelData>();
        string levels_dir_path = Path.Combine(Application.streamingAssetsPath, LEVELS_DIR_NAME);
        List<string> level_files = new List<string>(Directory.GetFiles(levels_dir_path));
        Debug.Log("Found "+ level_files.Count +" levels");
        level_files.Sort();
        foreach(string level_file in level_files)
        {
            if(level_file.EndsWith(".json"))
            {
                all_level_data.Add(ParseJSONFile<LevelData>(level_file));
            }
        }

        return all_level_data;
    }


    public static string ReadFile(string file_path)
    {
        return File.ReadAllText(file_path);
    }


    public static string GetUserDataFilePath()
    {
        return Path.Combine(Application.persistentDataPath, USER_DATA_FILE);
    }


    public static void WriteJSONFile<T>(string file_path, T structure)
    {
        WriteFile(file_path, JsonUtility.ToJson(structure));
    }


    public static void WriteFile(string file_path, string contents)
    {
        File.WriteAllText(file_path, contents);
    }


    public static T ParseJSON<T>(string raw_json)
    {
        Debug.Log("Raw json follows");
        Debug.Log(raw_json);
        return JsonUtility.FromJson<T>(raw_json);
    }


    private static string AddStreamingAssetsPath(string file_path)
    {
        if(!file_path.StartsWith(Application.streamingAssetsPath))
        {
            file_path = Path.Combine(Application.streamingAssetsPath, file_path);
        }
        return file_path;
    }


    public static bool FileExists(string file_path)
    {
        return File.Exists(file_path);
    }


    public static void DeleteFile(string file_path)
    {
        File.Delete(file_path);
    }
}
